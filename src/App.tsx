import * as React from 'react';
import {withRouter} from 'react-router-dom';
import './styles/App.css';
import logo from './styles/img/logo-grey.png';

// Components
import Main from './components/Main';
import Header from './components/Navigation/Header';



class App extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "",
    };


  }

  public componentDidMount() {
    console.log(this.props.location.pathname);
}
  
  public render() {
    return (
      <div className="App" > 
      {this.props.location.pathname !== "/dropoff" && 
        <Header />
    }
        <img className="main-logo" src={logo} />
        <Main />
      </div>
    );
  }
}

export default withRouter(App);
