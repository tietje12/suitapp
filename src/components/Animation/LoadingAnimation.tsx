import * as React from 'react';
import '../../styles/App.css';
import Loading from '../../styles/img/Rolling.gif';  


class LoadingAnimation extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      value: "",   
    };

  }


   
  public render() {
    return (
        <div>
            <img src={Loading} className="loader" />
             <p className="input-label text-center">Loading</p>
        </div>
    );
  }
}

export default LoadingAnimation;
