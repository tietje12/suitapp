import { FormValidation } from 'calidation';
import * as React from 'react';

import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Components
import LoadingAnimation from '../Animation/LoadingAnimation';
import DropdownObjects from '../Dropdown/DropdownObjects';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';
import ActionNotificationWithFunction from '../Notifications/ActionNotificationWithFunction';

// Axios for HTTP requests
import axios from 'axios';

class BulkCheckoutVests extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            name: "Esvagt",
            value: "",
            users: [],
            edit: false,
            data: null,
            loading: false,
            checkOutCompleted: false,
            checkoutList: []
        };

        // Remember to bind "this." methods
        this.getSuits = this.getSuits.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.addToUnkown = this.addToUnkown.bind(this);
        this.remove = this.remove.bind(this);
        this.completeCheckout = this.completeCheckout.bind(this);
        this.closeCompleteNotification = this.closeCompleteNotification.bind(this);
        this.creatBulkCheckout = this.creatBulkCheckout.bind(this);
        this.print = this.print.bind(this);

        this.formConfig = {
            location: {
                isRequired: 'Location is required'
            },
            id: {
                isRequired: 'ID is required'

            },
            checkOutComment: {

            }
        }
    }

    public closeCompleteNotification() {
        this.setState({
            checkOutCompleted: false
        });
        this.props.history.push({
            pathname: '/'
        })
    }

    public closeNotification() {
        this.setState({
            alert: false,
            alertNotVest: false
        });
    }


    public async completeCheckout() {

        const list = this.state.checkoutList;
        for (const item of list) {
            this.setState({
                loading: true
            });
            item.checkOutComment = this.state.checkOutComment;
            item.location = this.state.location;
            await this.updateSuit(item);

        }

        this.creatBulkCheckout();

        this.setState({
            loading: false,
            checkOutCompleted: true
        });


    }


    public updateSuit = (suit) => {
        return new Promise((resolve, reject) => {
            axios.post(Config.server + '/suit/' + suit.id, suit).then(
                (response) => {
                    console.log("updating....")
                    resolve(response)
                }).catch((error) => {
                    console.log(error);
                    reject(error);
                });
        })
    }


    public creatBulkCheckout() {
        const payload = {
            date: new Date(),
            location: this.state.location,
            performedBy: this.state.user,
            checkoutComment: this.state.checkOutComment,
            suits: this.state.checkoutList,
        }
        axios.post(Config.server + '/bulkCheckout/', payload).then(
            (response) => {
                console.log(response)
            }).catch((error) => {
                console.log(error);
            });

    }

    public print() {
        window.print();
    }

    public addToUnkown() {
        axios.post(Config.server + '/unknown/', { "id": this.state.unknownSuitID, "location": this.state.formFields.location }).then(
            response => {
                console.log(response);
                this.setState({
                    alert: false,
                })
                this.setState(prevState => ({
                    checkoutList: [...prevState.checkoutList, response.data]
                }))

            }).catch((error) => {
                console.log(error);
            });
    }
    public componentDidMount() {
        this.getLocations();
        this.setState({
            user: JSON.parse(sessionStorage.getItem('user') || '{}')
        })
    }

    public getLocations(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/location')
            .then(
                response => {
                    this.setState({
                        locations: response.data,
                        loading: false
                    })
                }
            )
    }


    public addToCheckoutList = ({ fields, errors, isValid }) => {
        if (isValid) {
            this.setState({
                formFields: fields,
            })
            this.getSuits(fields)


        }
    }


    // Get all suits
    public getSuits(fields): void {
        this.setState({
            loading: true
        })
        if (!fields.id) {
            fields.id = "0"
        }
        axios.get(Config.server + '/suit/' + fields.id)
            .then(
                response => {
                    console.log(response.data)
                    if (response.data === "No matching results found.") {
                        this.setState({
                            resultTitle: "Unkown suit",
                            resultMessage: "No matching results were found for ID " + fields.id + ". Do you want to add the suit to the unknown suits database?",
                            alert: true,
                            unknownSuitID: fields.id,
                            loading: false
                        })
                    } else if (response.data.suitType !== "5c76393cdd9c344c7b658ed2") {
                        this.setState({
                            resultTitle: "Not a vest",
                            resultMessage: "The suit you scanned is not a vest. Please try again.",
                            alertNotVest: true,
                            unknownSuitID: fields.id,
                            loading: false
                        })

                    } else {

                        axios.get(Config.server + '/servicePerformed/' + response.data.id + '/all')
                            .then(responseService => {
                                console.log(responseService)
                                response.data.service = responseService.data
                                this.setState(prevState => ({
                                    checkoutList: [...prevState.checkoutList, response.data],
                                    loading: false
                                }))
                            })




                    }
                }
            )
    }


    public remove(id) {

        const list = this.state.checkoutList;
        const modifiedArray = list.filter(item => item.id !== id)
        console.log(modifiedArray);

        this.setState({
            checkoutList: modifiedArray
        })

    }



    public render() {
        if (!this.state.locations) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2 className="box-center">Bulk Checkout Vests</h2>
                                    <FormValidation onSubmit={this.addToCheckoutList} config={this.formConfig} initialValues={{ checkOutComment: "", id: "", locations: this.state.locations.length ? this.state.locations[0]._id : undefined }}>
                                        {({ fields, errors, submitted, setField }) => (
                                            <>
                                                <p className="input-label text-center">Select Location</p>
                                                <DropdownObjects items={this.state.locations} label="" labelName="" name="location" onChange={(event) => { setField({ location: event.target.value }); this.setState({ location: event.target.value }); const index = event.nativeEvent.target.selectedIndex; this.setState({ locationDisplayName: event.nativeEvent.target[index].text }) }} error={submitted && errors.location ? errors.location : ''} />
                                                <p className="input-label text-center" style={{ marginBottom: "-30px" }}>Checkout Comment</p>
                                                <Textarea placeholder="Enter a comment" label="" labelName="" name="checkOutComment" error={submitted && errors.checkOutComment ? errors.checkOutComment : ''} onChange={(event) => this.setState({ checkOutComment: event.target.value })} />




                                                <p className="input-label text-center">Enter Suit Number</p>
                                                <InputAsync placeholder="Please scan the suit or enter the ID" label="" labelName="" name="id" error={submitted && errors.id ? errors.id : ''} />

                                                <div className="invalid-feedback">{errors.suitNumber}</div>
                                                <button type="submit" className="align-center">Add to Checkout List</button>
                                                {this.state.checkoutList.length > 0 &&
                                                    <button className="align-center" onClick={this.print} style={{ marginTop: "10px" }}>Print List</button>
                                                }
                                            </>
                                        )}
                                    </FormValidation>
                                    <div className="print-checkout">
                                        {this.state.checkoutList.length > 0 &&
                                            <p className="input-label text-center">Checkout List</p>
                                        }
                                        {this.state.checkoutList.length > 0 &&
                                            <p className="input-label text-center">Date: {new Date().toLocaleString()} | Location: {this.state.locationDisplayName}</p>
                                        }

                                        {this.state.checkoutList.length > 0 &&
                                            <p className="input-label text-center">Comment: {this.state.checkOutComment}</p>
                                        }
                                        <table className="table bulk-table" style={{ marginTop: "15px" }}>
                                            {this.state.checkoutList.length > 0 &&
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Size</th>
                                                        <th>Next Service</th>
                                                        <th className="no-print">Remove</th>
                                                    </tr>
                                                </thead>
                                            }
                                            <tbody>
                                                {this.state.checkoutList.map((item, index) =>
                                                    <React.Fragment key={index}>
                                                        <tr >
                                                            <td style={{paddingBottom: "0"}}>{item.id || "Unknown"}</td>
                                                            <td style={{paddingBottom: "0"}}>{item.size || "Unknown"}</td>
                                                            <td style={{paddingBottom: "0"}}>{new Date(item.nextService).toLocaleString() || "Unknown"}</td>
                                                            <td style={{paddingBottom: "0"}} className="no-print"><p className="link" onClick={() => this.remove(item.id)}>Remove</p></td>

                                                        </tr>
                                                        <tr >
                                                            <td colSpan={4} style={{borderTop: "none", paddingTop: "0"}}>
                                                                <p className="rowTitle">Last service {new Date(item.service[0].createdAt).toLocaleString()}:</p>
                                                                {item.service[0].tasks.map((item, index) =>

                                                                    <p className="rowP" key={index}>{item.label}: {item.check} /</p>
                                                                )}
                                                            </td>
                                                        </tr>
                                                    </React.Fragment>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                    {this.state.loading &&
                                        <LoadingAnimation />
                                    }
                                    {this.state.checkoutList.length > 0 &&
                                        <button onClick={this.completeCheckout} className="align-center">Complete Checkout</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.alert &&
                    <ActionNotificationWithFunction title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} action={this.addToUnkown} />
                }
                {this.state.alertNotVest &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }

                {this.state.checkOutCompleted &&
                    <ActionNotification title="Checkout completed" message="The bulk checkout has been completed." close={this.closeCompleteNotification} />
                }
            </div>
        );
    }
}

export default BulkCheckoutVests;
