import { FormValidation } from 'calidation';
import * as React from 'react';

import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Components
import LoadingAnimation from '../Animation/LoadingAnimation';
import DropdownObjects from '../Dropdown/DropdownObjects';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';
import ActionNotificationWithFunction from '../Notifications/ActionNotificationWithFunction';

// Axios for HTTP requests
import axios from 'axios';

class BulkCheckoutFlow extends React.Component<any, any>  {
  private formConfig: any;
  private nameInput: any;

  constructor(props: any) {
    super(props);
    this.state = {
      name: "Esvagt",
      value: "",
      users: [],
      edit: false,
      data: null,
      loading: false,
      checkOutCompleted: false,
      checkoutList: []
    };

    // Remember to bind "this." methods
    this.getSuits = this.getSuits.bind(this);
    this.closeNotification = this.closeNotification.bind(this);
    this.addToUnkown = this.addToUnkown.bind(this);
    this.remove = this.remove.bind(this);
    this.completeCheckout = this.completeCheckout.bind(this);
    this.closeCompleteNotification = this.closeCompleteNotification.bind(this);
    this.creatBulkCheckout = this.creatBulkCheckout.bind(this);
    this.print = this.print.bind(this);

    this.formConfig = {
      location: {
        isRequired: 'Location is required'
      },
      id: {
        isRequired: 'ID is required'

      },
      checkOutComment: {

      }
    }
  }

  public closeCompleteNotification() {
    this.setState({
      checkOutCompleted: false
    });
    this.props.history.push({
      pathname: '/'
    })
  }

  public closeNotification() {
    this.setState({
      alert: false
    });
  }


  public async completeCheckout() {

    const list = this.state.checkoutList;
    for (const item of list) {
      this.setState({
        loading: true
      });
      item.checkOutComment = this.state.checkOutComment;
      item.location = this.state.location;
      await this.updateSuit(item);

    }

    this.creatBulkCheckout();

    this.setState({
      loading: false,
      checkOutCompleted: true
    });


  }


  public updateSuit = (suit) => {
    return new Promise((resolve, reject) => {
      axios.post(Config.server + '/suit/' + suit.id, suit).then(
        (response) => {
          console.log("updating....")
          resolve(response)
        }).catch((error) => {
          console.log(error);
          reject(error);
        });
    })
  }


  public creatBulkCheckout() {
    const payload = {
      date: new Date(),
      location: this.state.location,
      performedBy: this.state.user,
      checkoutComment: this.state.checkOutComment,
      suits: this.state.checkoutList,
    }
    axios.post(Config.server + '/bulkCheckout/', payload).then(
      (response) => {
        console.log(response)
      }).catch((error) => {
        console.log(error);
      });

  }

  public print() {
    window.print();
  }

  public addToUnkown() {
    axios.post(Config.server + '/unknown/', { "id": this.state.unknownSuitID, "location": this.state.formFields.location }).then(
      response => {
        console.log(response);
        this.setState({
          alert: false,
        })
        this.setState(prevState => ({
          checkoutList: [...prevState.checkoutList, response.data]
        }))

      }).catch((error) => {
        console.log(error);
      });
  }
  public componentDidMount() {
    this.getLocations();
    this.setState({
      user: JSON.parse(sessionStorage.getItem('user') || '{}')
    })
  }

  public getLocations(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/location')
      .then(
        response => {
          this.setState({
            locations: response.data,
            loading: false
          })
        }
      )
  }


  public addToCheckoutList = ({ fields, errors, isValid }) => {
    if (isValid) {
      this.setState({
        formFields: fields,
      })
      this.getSuits(fields)


    }
  }


  // Get all suits
  public getSuits(fields): void {

    const specialString = fields.id.substring(0, 5)
    console.log(specialString);

    if (specialString === '¨C121') {
      fields.id = fields.id.substring(5);
      console.log("Contains c121")
    }

    // id = id.substring(4);
    this.nameInput.value = fields.id;
    console.log(fields.id)

    this.setState({
      loading: true
    })
    if (!fields.id) {
      fields.id = "0"
    }
    axios.get(Config.server + '/suit/' + fields.id)
      .then(
        response => {
          if (response.data === "No matching results found.") {
            this.setState({
              resultTitle: "Unkown suit",
              resultMessage: "No matching results were found for ID " + fields.id + ". Do you want to add the suit to the unknown suits database?",
              alert: true,
              unknownSuitID: fields.id,
              loading: false
            })
          } else {
            this.setState(prevState => ({
              checkoutList: [...prevState.checkoutList, response.data],
              loading: false
            }))

          }
        }
      )
  }


  public remove(id) {

    const list = this.state.checkoutList;
    const modifiedArray = list.filter(item => item.id !== id)
    console.log(modifiedArray);

    this.setState({
      checkoutList: modifiedArray
    })

  }



  public render() {
    if (!this.state.locations) {
      return null;
    }

    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  <h2 className="box-center">Bulk Checkout</h2>
                  <FormValidation onSubmit={this.addToCheckoutList} config={this.formConfig} initialValues={{ checkOutComment: "", id: "", locations: this.state.locations.length ? this.state.locations[0]._id : undefined }}>
                    {({ fields, errors, submitted, setField }) => (
                      <>
                        <p className="input-label text-center">Select Location</p>
                        <DropdownObjects items={this.state.locations} label="" labelName="" name="location" onChange={(event) => { setField({ location: event.target.value }); this.setState({ location: event.target.value }); const index = event.nativeEvent.target.selectedIndex; this.setState({ locationDisplayName: event.nativeEvent.target[index].text }) }} error={submitted && errors.location ? errors.location : ''} />
                        <p className="input-label text-center" style={{ marginBottom: "-30px" }}>Checkout Comment</p>
                        <Textarea placeholder="Enter a comment" label="" labelName="" name="checkOutComment" error={submitted && errors.checkOutComment ? errors.checkOutComment : ''} onChange={(event) => this.setState({ checkOutComment: event.target.value })} />




                        <p className="input-label text-center">Enter Suit Number</p>
                        <InputAsync ref={(input) => { this.nameInput = input; }} placeholder="Please scan the suit or enter the ID" label="" labelName="" name="id" error={submitted && errors.id ? errors.id : ''} />

                        <div className="invalid-feedback">{errors.suitNumber}</div>
                        <button type="submit" className="align-center">Add to Checkout List</button>
                        {this.state.checkoutList.length > 0 &&
                          <button className="align-center" onClick={this.print} style={{ marginTop: "10px" }}>Print List</button>
                        }
                      </>
                    )}
                  </FormValidation>
                  <div className="print-checkout">
                    {this.state.checkoutList.length > 0 &&
                      <p className="input-label text-center">Checkout List</p>
                    }
                    {this.state.checkoutList.length > 0 &&
                      <p className="input-label text-center">Date: {new Date().toLocaleString()} | Location: {this.state.locationDisplayName}</p>
                    }

                    {this.state.checkoutList.length > 0 &&
                      <p className="input-label text-center">Comment: {this.state.checkOutComment}</p>
                    }
                    <table className="table" style={{ marginTop: "15px" }}>
                      {this.state.checkoutList.length > 0 &&
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Size</th>
                            <th>Shoe Size</th>
                            <th>Next Service</th>
                            <th>Remove</th>
                          </tr>
                        </thead>
                      }
                      <tbody>
                        {this.state.checkoutList.map((item, index) =>
                          <tr key={index}>
                            <td>{item.id || "Unknown"}</td>
                            <td>{item.size || "Unknown"}</td>
                            <td>{item.shoeSize || "Unknown"}</td>
                            <td>{new Date(item.nextService).toLocaleString() || "Unknown"}</td>
                            <td><p className="link" onClick={() => this.remove(item.id)}>Remove</p></td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                  {this.state.loading &&
                    <LoadingAnimation />
                  }
                  {this.state.checkoutList.length > 0 &&
                    <button onClick={this.completeCheckout} className="align-center">Complete Checkout</button>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.alert &&
          <ActionNotificationWithFunction title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} action={this.addToUnkown} />
        }

        {this.state.checkOutCompleted &&
          <ActionNotification title="Checkout completed" message="The bulk checkout has been completed." close={this.closeCompleteNotification} />
        }
      </div>
    );
  }
}

export default BulkCheckoutFlow;
