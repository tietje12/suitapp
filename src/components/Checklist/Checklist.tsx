import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';


// Axios for HTTP requests
import axios from 'axios';
import { Link } from 'react-router-dom';

class Checklist extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "",
      data: [],
      loading: false,   
    };


  }

  public componentDidMount() {
    this.getChecklists();
    }

  public handleChange(event) {
    console.log(event);
    this.setState({value: event.target.value});
  }


    // Get all checklists
  public getChecklists(): void{
    this.setState({
      loading:true
    })
    axios.get(Config.server + '/checklist')
    .then(
        response =>{
          this.setState({
            data:response.data,
            loading: false
          })
        } 
      ) 
  }


  
  public render() {
    return (
        
      <div className="App" > 
        <div className="container">
          <div className="wrapper">
            <div className="row">
                  <div className="col-sm-12">
                      <div className="box">
                      <h2>Checklists</h2>
                      <ul className="infoList">
                        {this.state.data.map((item, index) =>
                            <li key={index}>
                                <Link to={`/checklist/${item._id}`}>
                                    {item.title}
                                </Link>
                            </li>
                            

                        )}
                        </ul>
                        <Link to='/createChecklist'><button>Create new checklist</button></Link>
                      </div>
                  </div>
              </div>
          
          
          
          </div>
        </div>
      </div>
    );
  }
}

export default Checklist;
