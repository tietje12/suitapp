import MultiSelect from "@kenshooui/react-multi-select";
import * as React from 'react';
import {default as Config} from '../../config/config'
import '../../styles/App.css';
// Components

import DropdownObjects from '../Dropdown/DropdownObjects';
import Input from '../Inputs/Input';
import InputFile from '../Inputs/InputFile';
import ActionNotification from '../Notifications/ActionNotification';




// Axios for HTTP requests
import axios from 'axios';

class CreateChecklist extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            selectedItems: [],
            items: [],
            checklist: {
                tasks: []
            }
        };

        // Bind functions
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
        this.createChecklist = this.createChecklist.bind(this);
        this.closeNotification = this.closeNotification.bind(this);

    }

    public componentDidMount() {
        this.getSuitTypes();
        this.getTasks();

    }




    public getSuitTypes(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suitType')
            .then(
                response => {
                    this.setState({
                        suitTypes: response.data,
                        loading: false
                    })
                }
            )
    }

    public getTasks(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/serviceTasks')
            .then(
                response => {
                    console.log(response);
                    const array: any[] = [];
                    response.data.map((item, index) =>
                        array.push({ "id": index, "label": item.title + "(" + item.comment + ")", "_id": item._id })
                    )
                    this.setState({
                        items: array,
                        loading: false
                    })
                }
            )
    }

    // Close modale
    public closeNotification() {
        this.setState({
            success: false
        });
        // Redirect to new checklist
        // this.props.history.push("/checklist/" + this.state.resultResponse.data._id);
    }

    public createChecklist(): void {
        const updatedValue = Object.assign({}, this.state.checklist, { tasks: this.state.selectedItems, title: this.state.checklist.title });
        this.setState({ checklist: updatedValue });

        if (this.state.image) {
            // Create formdata for fileupload
            const formData = new FormData();
            formData.append('myImage', this.state.image);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            };
            axios.post(Config.server + "/upload/form", formData, config)
                .then((res) => {
                    const payload = {
                        tasks: this.state.selectedItems,
                        title: this.state.checklist.title,
                        suitType: this.state.checklist.suitType,
                        image: res.data
                    }
                    axios.post(Config.server + '/checklist', payload).then(
                        response => {
                            console.log(response)
                            this.setState({
                                success: true,
                                resultResponse: response,
                                resultTitle: "Success",
                                resultMessage: "Checklist has been created."
                            })
                        }).catch((error) => {
                            this.setState({
                                success: true,
                                resultResponse: error,
                                resultTitle: "Error",
                                resultMessage: error
                            })
                        });


                }).catch((error) => {
                    console.log(error);
                    this.setState({
                        success: true,
                        resultResponse: error,
                        resultTitle: "Error",
                        resultMessage: error
                    })
                });
        } else {
            const payload = {
                tasks: this.state.selectedItems,
                title: this.state.checklist.title,
                suitType: this.state.checklist.suitType,
            }
            axios.post(Config.server + '/checklist', payload).then(
                response => {
                    console.log(response)
                    this.setState({
                        success: true,
                        resultResponse: response,
                        resultTitle: "Success",
                        resultMessage: "Checklist has been created."
                    })
                }).catch((error) => {
                    this.setState({
                        success: true,
                        resultResponse: error,
                        resultTitle: "Error",
                        resultMessage: error
                    })
                });

        }

    }

    // Handle change of value events
    public handleChange(event) {
        // Make a copy of the object in the state
        if (event.target.files) {
            console.log(event.target.files);
            this.setState({ image: event.target.files[0] });
        }
        const updatedValue = Object.assign({}, this.state.checklist, { [event.target.name]: event.target.value });

        // Now we can update the state
        this.setState({ checklist: updatedValue });
    }

    // Handle change in multiselect
    public handleChangeSelect(selectedItems) {
        this.setState({ selectedItems });
    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-8">
                                <div className="box" onChange={this.handleChange}>
                                    <h2>Create new checklist</h2>
                                    <Input placeholder="Enter Title" label="Title" labelName="title" name="title" />
                                    <DropdownObjects items={this.state.suitTypes} label="Suit Type" labelName="suitType" name="suitType" />
                                    <p className="input-label">Add tasks</p>
                                    <MultiSelect
                                        items={this.state.items}
                                        selectedItems={this.state.selectedItems}
                                        onChange={this.handleChangeSelect}
                                    />
                                    <InputFile label="Add image" labelName="image" name="image" />
                                    <button onClick={this.createChecklist}>Create checklist</button>
                                    {this.state.success &&
                                        <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                                    }
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );

    }
}

export default CreateChecklist;
