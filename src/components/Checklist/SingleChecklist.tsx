import * as React from 'react';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'

import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';
import ActionNotification from '../Notifications/ActionNotification';


class SingleChecklist extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
                tasks: []
            },
            suitType: {}
        };

        this.deleteChecklist = this.deleteChecklist.bind(this);
        this.closeNotification = this.closeNotification.bind(this);

    }

    public componentWillMount() {
        this.getChecklist(this.props.match.params.id);
    }


    // Close modale
    public closeNotification() {
        this.setState({
            success: false
        });
        this.props.history.push("/checklist");
    }

    // Delete checklist
    public deleteChecklist(): void {
        axios.delete(Config.server + '/checklist/' + this.props.match.params.id).then(
            response => {
                console.log(response);
                this.setState({
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "Checklist has been deleted."
                })

            }).catch((error) => {
                console.log(error);
                this.setState({
                    success: true,
                    resultResponse: error,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });
    }

    // Get single checklist
    public getChecklist(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/checklist/' + id)
            .then(
                response => {
                    if (response.data) {
                        this.setState({
                            data: response.data,
                            loading: false
                        })
                        this.getSuitType(response.data.suitType)
                    }

                }
            )
    }

    // Get single suittype
    public getSuitType(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suittype/' + id)
            .then(
                response => {
                    if (response.data) {
                        this.setState({
                            suitType: response.data
                        })
                    }

                }
            )
    }


    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>{this.state.data.title}</h2>
                                    <p className="input-label">Suit Type</p>
                                    <p>{this.state.suitType.title}</p>
                                    <p className="input-label">Tasks</p>
                                    <ul className="infoList">
                                        {this.state.data.tasks.map((item, index) =>
                                            <li key={index}>
                                                {item.label}
                                            </li>
                                        )}
                                    </ul>
                                    <p className="input-label">Image</p>
                                    {this.state.data.image &&
                                        <img style={{ width: "200px", margin: "10px", display: "block" }} src={`data:image/png;base64,${this.state.data.image}`} />
                                    }
                                    {!this.state.data.image &&
                                       <p>No image has been attached</p>
                                    }
                                    <button className="button" onClick={this.deleteChecklist}>Delete checklist</button>
                                    <Link to={`/checklist/${this.state.data._id}/edit`}>
                                        <button className="button">Edit</button>
                                    </Link>

                                    {this.state.success &&
                                        <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                                    }
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        );
    }
}

export default SingleChecklist;
