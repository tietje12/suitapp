import * as React from 'react';
import '../../styles/App.css';

class Dropdown extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = { value: '' };

  }

  public render() {
    return (
      <div>
        <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
        <select name={this.props.name} id={this.props.labelName} value={this.props.value} onChange={this.props.onChange}>
          {!this.props.value && <option value="" disabled={true} selected={true}>-- Choose {this.props.label} --</option>}
          {this.props.items.map((item, index) =>
            <option key={index} value={item}>{item}</option>
          )}
        </select>
      </div>
    );

  }
}

export default Dropdown;