import * as React from 'react';
import '../../styles/App.css';


class Dropdown extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = { value: '' };

  }


  public selectValue(items, value) {
    let options;
    options = items.map((item, index) => {
      return <option key={index} value={item._id}>{item.title}</option>
    })
    return options;
  }

  public render() {
    if (this.props.items && this.props.value) {
      return (
        <div>
          <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
          <select name={this.props.name} id={this.props.labelName} onChange={this.props.onChange} value={this.props.value}>
            {this.selectValue(this.props.items, "test")}
          </select>
        </div>
      );
    } else if (this.props.items && !this.props.value) {
      return (
        <div>
          <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
          <select name={this.props.name} id={this.props.labelName} onChange={this.props.onChange}>
            <option value="" disabled={true} selected={true}>-- Choose {this.props.label} --</option>
            {this.props.items.map((item, index) =>
              <option key={index} value={item._id}>{item.title}</option>
            )}
          </select>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Dropdown;
