import * as React from 'react';
import {default as Config} from '../../config/config'

import '../../styles/App.css';


// Components

import LoadingAnimation from '../Animation/LoadingAnimation';
import ActionNotification from '../Notifications/ActionNotification';


// Axios for HTTP requests
import axios from 'axios';


class Dropoff extends React.Component<any, any>  {

  constructor(props: any){ 
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "",
      users: [],
      edit: false,
      data: null,
      loading: false,   
    };

    // Remember to bind "this." methods
    this.handleChange = this.handleChange.bind(this);
    this.getSuits = this.getSuits.bind(this);
    this.searchSuit = this.searchSuit.bind(this);
    this.closeNotification = this.closeNotification.bind(this);
  }


  public handleChange(event) {
    this.setState({value: event.target.value});
  }

  public searchSuit() {
    this.getSuits(this.state.value);
  }
  public closeNotification(){

    this.setState({
        success: false
    });
    window.location.reload();
    }

  public getSuits(id): void{
    this.setState({
      loading:true
    })
    if(!id){
        id = "0"
    }
    axios.get(Config.server + '/suit/' + id)
    .then(
        response =>{
          console.log(response)
          if(response.data === "No matching results found."){
            this.createUnkownSuit(id)
          } else {
            this.updateSuits({"id": id, "location": "5c4584af402d672544a91dd9"})
            
        } 
      }
      )
  }

// Create entry for unknown suit
public createUnkownSuit(id): void{
  axios.post(Config.server + '/unknown/', {"id": id}).then( 
    response => {
        console.log(response);
        this.setState({
            success: true,
            resultResponse: response,
            resultTitle: "Success",
            loading:false,
            resultMessage: "Your suit has been registered but does not exist in our system. Please place the suit at the UNKNOWN shelf"
        })

    }).catch((error)=>{
        console.log(error);
        this.setState({
            success: true,
            resultTitle: "Error",
            resultMessage: error
        })
    });
  }

 

  // Update suit
  public updateSuits(suit): void{
    console.log(suit);
    axios.post(Config.server + '/suit/' + suit.id, suit).then( 
      response => {
        this.setState({
          success: true,
          loading:false,
          resultTitle: "Registration success",
          resultMessage: "Your suit has been registered, please proceed to drop off.",
          })

  }).catch((error)=>{
    this.setState({
      success: true,
      resultTitle: "Error",
      resultMessage: error
  })
 });
  }
  public render() {
    return (
      <div className="App" > 
        <div className="container">
          <div className="wrapper">
            <div className="row">
                  <div className="col-sm-12">
                      <div className="box">
                            <p className="input-label text-center">Enter Suit Number</p>
                            <input className="text-center" onChange={this.handleChange}/>
                            <button className="align-center" onClick={this.searchSuit}>Drop off</button>
                      </div>
                  </div>
              </div>
              <div className="row">
                  <div className="col-sm-12">
                    <div className="box">
                      { this.state.loading &&
                        <LoadingAnimation />
                      }
                      {this.state.success &&
                        
                        <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification}/>
                        }
   
                    </div>
                  </div>
              </div>
          
          
          
          </div>
        </div>
      </div>
    );
  }
}

export default Dropoff;
