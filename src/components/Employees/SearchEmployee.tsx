import { FormValidation } from 'calidation';
import * as React from 'react';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { default as Config } from '../../config/config'

import '../../styles/App.css';

import axios from 'axios';
// import { userInfo } from 'os';

class SearchEmployee extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            allowNew: false,
            loading: false,
            employees: []
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.goToSuit = this.goToSuit.bind(this);
        this.handleSearch = this.handleSearch.bind(this);

        this.formConfig = {
            id: {
                isRequired: 'Employee is required'
            }
        }
    }



    public handleSearch = (query) => {
        this.setState({ loading: true });

        axios.post(Config.server + '/ocs/', { "value": query }).then(response => {
            const firstKey = Object.keys(response.data[0]);
            const data = JSON.parse(response.data[0][firstKey[0]]);
            this.setState({
                employees: data.Employee,
                loading: false
            })

        }).catch((error) => {
            console.log(error);
        });
    }

    public onSubmit = ({ fields, errors, isValid }) => {
        console.log(fields);
        if (isValid) {
            this.setState({
                result: fields.user[0],
                loading: false
            })
            this.getAssignedSuits(fields.user[0].EmployeeID);
        }
    }

    // Get single guest user
    public getAssignedSuits(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/assignedToEmp/' + id)
            .then(
                response => {
                    console.log(response);
                    this.setState({
                        suits: response.data,
                        loading: false
                    })

                    axios.get(Config.server + '/suit/' + id + '/inUseTemp')
                        .then(
                            response => {
                                console.log(response);
                                const newArray = this.state.suits
                                for (const item of response.data) {
                                    item.InUseTemp = "Yes";
                                    newArray.push(item);
                                }
                                this.setState({
                                    suits: newArray,
                                    loading: false
                                })
                            }
                        )
                }
            )


    }
    public goToSuit(idn) {
        this.props.history.push({
            pathname: '/result',
            state: { id: idn }
        })
    }
    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <FormValidation onSubmit={this.onSubmit} config={this.formConfig}>
                                        {({ fields, errors, submitted, setField }) => (
                                            <>
                                                <h2>Find employee data</h2>

                                                <AsyncTypeahead
                                                    labelKey={option => `${option.EmployeeID}: ${option.EmployeeName}`}
                                                    multiple={false}
                                                    options={this.state.employees}
                                                    minLength={3}
                                                    onSearch={this.handleSearch}
                                                    isLoading={this.state.loading}
                                                    placeholder="Search for Employee..."
                                                    name="id"
                                                    isInvalid={submitted && errors.id ? true : false}
                                                    onChange={(value) => { console.log(value); setField({ id: value.EmployeeId, user: value }); }}
                                                />

                                                <button type="submit" className="button">View Employee</button>

                                            </>
                                        )}

                                    </FormValidation>
                                    {this.state.result &&
                                        <div className="row">
                                            <div className="col-md-4">
                                                <p className="input-label">Name</p>
                                                <p>{this.state.result.EmployeeName}</p>
                                                <p className="input-label">ID</p>
                                                <p>{this.state.result.EmployeeID}</p>
                                                <p className="input-label">Sex</p>
                                                <p>{this.state.result.EmployeeSex}</p>
                                                <p className="input-label">Height</p>
                                                <p>{this.state.result.EmployeeHeight}</p>
                                                <p className="input-label">Weight</p>
                                                <p>{this.state.result.EmployeeWeight}</p>
                                            </div>
                                            <div className="col-md-2">
                                                <p className="input-label">Birthdate</p>
                                                <p>{this.state.result.EmployeeBirthdate}</p>
                                                <p className="input-label">Shoe Size</p>
                                                <p>{this.state.result.EmployeeBootSize}</p>
                                                <p className="input-label">Country</p>
                                                <p>{this.state.result.EmployeeCountry}</p>
                                                <p className="input-label">Company</p>
                                                <p>{this.state.result.EmployeeCompany}</p>
                                                <p className="input-label">Nationality</p>
                                                <p>{this.state.result.EmployeeNationality}</p>
                                            </div>
                                            <div className="col-md-6">
                                                {this.state.suits && this.state.suits.length > 0 &&
                                                    <div className="col-sm-12">
                                                        <p className="input-label">Assigned suits</p>
                                                        <table className="table">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">ID</th>
                                                                    <th scope="col">Type</th>
                                                                    <th scope="col">In Use Temp</th>
                                                                    <th scope="col">Link</th>
                                                                    
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {this.state.suits.map((item, index) =>
                                                                    <tr key={index}>
                                                                        <td>{item.id}</td>
                                                                        <td>{item.type}</td>
                                                                        <td>
                                                                            {item.InUseTemp}
                                                                        </td>
                                                                        <td>
                                                                            <a href="" onClick={() => this.goToSuit(item.id)}>Go to suit</a>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                )}
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchEmployee;