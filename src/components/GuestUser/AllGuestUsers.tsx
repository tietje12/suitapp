import * as React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

// Components
import LoadingAnimation from '../Animation/LoadingAnimation';

// Axios for HTTP requests
import axios from 'axios';

class AllGuestUsers extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {
      name: "Esvagt",
      data: [],
      loading: false,
      suitTypes: [],
      locations: []
    };

    // Remember to bind "this." methods
    this.getUsers = this.getUsers.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  public componentDidMount() {
    this.getUsers();
  }

  public onClick(e, row, rowIndex) {
    if (!row.id) {
      return;
    }

    this.props.history.push({
      pathname: '/guestUser/' + row.id,
      state: { id: row.id }
    });
  }

  // Get all available suits
  public getUsers(): void {
    this.setState({
      loading: true
    });

    axios.get(Config.server + '/guest')
      .then(
        response => {
          if (response.data === "No matching results found.") {
            this.setState({
              data: [],
              loading: false
            })
          } else {
            this.setState({
              data: response.data,
              loading: false
            });
          }
        }
      )
  }

  public dateFormatter(cell, row) {
    return new Date(cell).toLocaleString();
  }



  public render() {

    const columns = [
      {
        dataField: 'id',
        text: 'ID',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'name',
        text: 'Name',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'size',
        text: 'Size',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'shoeSize',
        text: 'Shoesize',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'email',
        text: 'Email',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'phone',
        text: 'Phone',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'createdAt',
        text: 'Created',
        sort: true,
        formatter: this.dateFormatter,
        filterValue: this.dateFormatter,
        filter: textFilter()

      },
    ];

    const defaultSort = [{
      dataField: 'id',
      order: 'asc'
    }];

    const rowEvents = {
      onClick: this.onClick
    };

    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                <h2>Guest User Overview</h2>
                <Link to={`/guestUser/create`}>
                    <button className="button">Create Guest User</button>
                  </Link>
  
                  {this.state.loading &&
                    <LoadingAnimation />
                  }
                  {this.state.data && this.state.data.length ?
                    <BootstrapTable keyField='_id' rowEvents={rowEvents} data={this.state.data} bootstrap4={true} columns={columns} striped={true} condensed={true} hover={true} bordered={false} defaultSorted={defaultSort} filter={filterFactory()} noDataIndication="No available suits." />
                    : ''}
                    
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AllGuestUsers; // 
