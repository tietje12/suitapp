import { FormValidation } from 'calidation';
import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Components
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';


// Datepicker

import "react-datepicker/dist/react-datepicker.css";

// Axios for HTTP requests
import axios from 'axios';

class CreateGuestUser extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            isOpen: false,
            isLocationOpen: false,
            success: false
        };

        this.formConfig = {
            size: {
                isRequired: 'Size is required',
            },
            shoeSize: {
                isRequired: 'Shoesize is required',
            },
            name: {
                isRequired: 'name is required'
            },
            phone: {

            },
            comment: {

            },
            id: {
                isRequired: 'Id is required'
            },
            email: {
                isRequired: 'Email is required'
            }
        }

        // Bind functions
        this.createGuestUser = this.createGuestUser.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
    }




    public closeNotification() {
        this.setState({
            success: false
        });
        if (this.state.resultTitle !== "Error") {
            if (this.props.location.state) {
                this.props.history.push({
                    pathname: '/suit/' + this.props.location.state.id + '/assign/guest',
                })
            } else {
                this.props.history.push({
                    pathname: '/guestUsers',
                })
            }
        }

    }




    public createGuestUser = ({ fields, errors, isValid }) => {
        if (isValid) {
            fields.guest = true;
            axios.post(Config.server + '/guest/', fields).then(response => {
                this.setState({
                    suit: response.data,
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "A new guest user has succesfully been created. Please proceed by clicking on ok."
                })



            }).catch((error) => {
                this.setState({
                    success: true,
                    resultTitle: "Error",
                    resultMessage: "User with ID " + fields.id + " already exist. Please choose a different ID."
                })
            });
        }
    }




    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-8">
                                <FormValidation onSubmit={this.createGuestUser} config={this.formConfig}>
                                    {({ fields, errors, submitted, setField }) => (
                                        <>
                                            <div className="box">
                                                <h2>Create new guest user</h2>
                                                <InputAsync placeholder="Enter fullname" label="Name" labelName="name" name="name" value={fields.name} error={submitted && errors.name ? errors.name : ''} />
                                                <InputAsync placeholder="Enter phone number" label="Phone" labelName="phone" name="phone" value={fields.phone} />
                                                <InputAsync placeholder="Enter email" label="Email" labelName="email" name="email" value={fields.email} error={submitted && errors.email ? errors.email : ''} />
                                                <InputAsync placeholder="Enter size" label="Size" labelName="size" name="size" value={fields.size} error={submitted && errors.size ? errors.size : ''} />
                                                <InputAsync placeholder="Enter shoe size" label="Shoe Size" labelName="shoeSize" name="shoeSize" value={fields.shoeSize} error={submitted && errors.shoeSize ? errors.shoeSize : ''} />
                                                <InputAsync placeholder="Enter ID" label="Id" labelName="id" name="id" value={fields.id} error={submitted && errors.id ? errors.id : ''} />
                                                <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" value={fields.comment} error={submitted && errors.comment ? errors.comment : ''} />


                                                <button type="submit">Create Guest User</button>
                                                {this.state.success &&
                                                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                                                }
                                            </div>
                                        </>
                                    )}
                                </FormValidation>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );

    }
}

export default CreateGuestUser;
