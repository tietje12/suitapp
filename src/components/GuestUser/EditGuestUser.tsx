import { FormValidation } from 'calidation';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';

class EditGuestUser extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
        };

        this.formConfig = {
            name: {

            },
            email: {

            },
            size: {

            },
            shoeSize: {

            },
            phone: {

            },
            id: {

            },
        }

        // Remember to bind "this."
        this.updateGuestUser = this.updateGuestUser.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
    }

    public closeNotification() {
        this.setState({
            success: false
        });
        if (this.state.resultTitle !== "Error") {

            this.props.history.push({
                pathname: '/guestUser/' + this.props.location.state.data.id,
            })
        }

    }

    // Call parent function parsed as prop
    public updateGuestUser = ({ fields, errors, isValid }) => {
        if (isValid) {
            const updatedGuest = Object.assign({}, this.props.location.state.data, fields);

            axios.post(Config.server + '/guest/' + this.props.location.state.data.id, updatedGuest).then(
                (response) => {

                    this.setState({
                        success: true,
                        resultResponse: response,
                        resultTitle: "Success",
                        resultMessage: "Guest user has been updated."
                    })

                }).catch((error) => {
                    this.setState({
                        success: true,
                        resultTitle: "Error",
                        resultMessage: error
                    })
                });
        }
    }










    public onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    public render() {
        if (!this.props.location.state.data) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <FormValidation onSubmit={this.updateGuestUser} config={this.formConfig} initialValues={this.props.location.state.data}>
                                    {({ fields, errors, submitted, setField }) => (
                                        <>
                                            <div className="box">
                                                <h2>Edit Guest User</h2>
                                                <InputAsync placeholder="Enter name" label="Name" labelName="name" name="name" value={fields.name} error={submitted && errors.name ? errors.name : ''} />
                                                <InputAsync placeholder="Enter email" label="email" labelName="email" name="email" value={fields.email} error={submitted && errors.email ? errors.email : ''} />
                                                <InputAsync placeholder="Enter size" label="size" labelName="size" name="size" value={fields.size} error={submitted && errors.size ? errors.size : ''} />
                                                <InputAsync placeholder="Enter shoe size" label="shoeSize" labelName="shoeSize" name="shoeSize" value={fields.shoeSize} error={submitted && errors.shoeSize ? errors.shoeSize : ''} />
                                                <InputAsync placeholder="Enter phone" label="phone" labelName="phone" name="phone" value={fields.phone} error={submitted && errors.phone ? errors.phone : ''} />
                                                <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" value={fields.comment} error={submitted && errors.comment ? errors.comment : ''} />

                                                <button className="button" type="submit">Update</button>
                                                <Link to={{
                                                    pathname: '/guestUser/' + this.props.location.state.data.id,

                                                }}>
                                                    <button className="button">Cancel</button>
                                                </Link>
                                            </div>
                                        </>
                                    )}
                                </FormValidation>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default EditGuestUser;
