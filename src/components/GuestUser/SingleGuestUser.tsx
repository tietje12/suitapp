import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import ActionNotification from '../Notifications/ActionNotification';

// Axios for HTTP requests
import axios from 'axios';


class SingleGuestUser extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {}
        };

        this.deleteGuestUser = this.deleteGuestUser.bind(this);
        this.goToSuit = this.goToSuit.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.editGuestUser = this.editGuestUser.bind(this);

    }
    public closeNotification() {
        this.setState({
            success: false
        });
        this.props.history.push({
            pathname: '/guestUsers'
        })
    }
    public componentWillMount() {
        this.getGuestUser(this.props.match.params.id);
    }

    public goToSuit(idn) {
        this.props.history.push({
            pathname: '/result',
            state: { id: idn }
          })
    }

    public editGuestUser() {
        this.props.history.push({
            pathname: '/guestUsers/' + this.state.data.id + '/edit',
            state: { data: this.state.data}
          })
    }


    // Get single guest user
    public getGuestUser(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/guest/' + id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                    this.getAssignedSuits(id);
                }
            )
    }


    // Get single guest user
    public getAssignedSuits(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/assignedTo/' + id)
            .then(
                response => {
                    this.setState({
                        suits: response.data,
                        loading: false
                    })
                }
            )
    }


    // Delete single guest user
    public deleteGuestUser(): void {
        this.setState({
            loading: true
        })
        axios.delete(Config.server + '/guest/' + this.state.data.id)
            .then(
                response => {
                    this.setState({
                        success: true,
                        resultResponse: response,
                        resultTitle: "Success",
                        resultMessage: "Guest user has succesfully been deleted. Please proceed by clicking on ok."
                    })
                }
            )
    }


    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Guest User: {this.state.data.name}</h2>

                                    <div className="row">
                                        <div className="col-sm-6">
                                            <p className="input-label">Name</p>
                                            <p>{this.state.data.name}</p>
                                            <p className="input-label">Email</p>
                                            <p>{this.state.data.email}</p>
                                            <p className="input-label">Size</p>
                                            <p>{this.state.data.size}</p>
                                            <p className="input-label">Shoe Size</p>
                                            <p>{this.state.data.shoeSize}</p>
                                            <p className="input-label">Phone</p>
                                            <p>{this.state.data.phone}</p>
                                            <p className="input-label">ID</p>
                                            <p>{this.state.data.id}</p>
                                            <p className="input-label">Comment</p>
                                            <p>{this.state.data.comment}</p>
                                            <p className="input-label">Date created</p>
                                            <p>{new Date(this.state.data.createdAt).toLocaleString()}</p>
                                            <button className="button" onClick={this.editGuestUser}>Edit guest user</button>
                                            <button className="button" onClick={this.deleteGuestUser}>Delete guest user</button>
                                        </div>
                                        {this.state.suits && this.state.suits.length > 0 &&
                                            <div className="col-sm-6">
                                                <p className="input-label">Assigned suits</p>
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">ID</th>
                                                            <th scope="col">Type</th>
                                                            <th scope="col">Link</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.suits.map((item, index) =>
                                                            <tr key={index}>
                                                                <td>{item.id}</td>
                                                                <td>{item.type}</td>
                                                                <td>
                                                                    <a href="" onClick={() => this.goToSuit(item.id)}>Go to suit</a>
                                                                </td>
                                                            </tr>
                                                        )}
                                                    </tbody>
                                                </table>

                                            </div>
                                        }
                                       

                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default SingleGuestUser;
