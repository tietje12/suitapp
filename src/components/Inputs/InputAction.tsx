import * as React from 'react';
import '../../styles/App.css';


class InputAction extends React.Component<any, any>  {
    private textInput: React.RefObject<HTMLInputElement>;
    constructor(props: any){
        super(props);
        this.textInput = React.createRef();
        this.state = {
            value: ''
        
        };
        this.action = this.action.bind(this);
   

    }

    public action(): void{ 
        console.log(this.textInput)
        console.log(this.textInput.current!.value)
        this.props.action(this.textInput.current!.value);
        this.textInput.current!.value = ""
        }

  public render() {
    return (
        <div>
            <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
            <input className="input-add" type="text" placeholder={this.props.placeholder}  value={this.props.value} name={this.props.name} ref={this.textInput} />
            <button className="button-add" onClick={this.action}>+</button>
      </div>
    );
    
  }
}

export default InputAction;
