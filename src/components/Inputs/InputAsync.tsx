import * as React from 'react';
import '../../styles/App.css';

class InputAsync extends React.Component<any, any>  {
  public inputRef = React.createRef<HTMLInputElement>();
  constructor(props: any) {
    super(props);
    this.state = { value: '' };
  }

  public render() {
    const className = this.props.error ? 'is-invalid form-control' : 'form-control';

    return (
      <div>
        <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
        <input ref={this.inputRef} type="text" placeholder={this.props.placeholder} name={this.props.name} value={this.props.value} className={className} />
        <div className="invalid-feedback">{this.props.error}</div>
      </div>
    );
  }
}

export default InputAsync;
