import * as React from 'react';
import '../../styles/App.css';


class InputFile extends React.Component<any, any>  {
    constructor(props: any){
        super(props);
        this.state = {value: ''};

    }

  public render() {
    return (
        <div>
            <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
            <input type="file" placeholder={this.props.placeholder}  value={this.props.value} name={this.props.name}/>
      </div>
    );
    
  }
}

export default InputFile;
 