import * as React from 'react';
import '../../styles/App.css';


class Textarea extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = { value: '' };
  }

  public render() {
    const className = this.props.error ? 'is-invalid form-control' : 'form-control';
    
    return (
      <div>
        <label htmlFor={this.props.labelName} className="input-label col-form-label">{this.props.label}</label>
        <textarea placeholder={this.props.placeholder} name={this.props.name} value={this.props.value} className={className} onChange={this.props.onChange}/>
        <div className="invalid-feedback">{this.props.error}</div>
      </div>
    );
  }
}

export default Textarea;