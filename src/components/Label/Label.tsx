import * as React from 'react';
import Barcode from 'react-barcode';
import '../../styles/App.css';
import img from '../../styles/img/logo.png';


class Label extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {

    };

    this.print = this.print.bind(this);

  }

  public print() {
    window.print();
  }

  public render() {
    if (!this.props.barcode) {
      return null;
    }

    return (
      <div>
        
        <div className="label-area">
        
          <div className="print" id="print">
            <img className="label-logo" src={img} />
            <h5 className="text-center label-title no-margin label-vessel" style={{textTransform:"uppercase"}}>{this.props.vessel}</h5>
            <h5 className="text-center label-title no-margin">{this.props.assignedTo}</h5>
            <h5 className="text-center label-title no-margin label-number" style={{fontSize: "27px"}}>{this.props.assignedToNumber}</h5>
            <Barcode width={2} height={60} value={this.props.barcode} />
            <div className="label-bottom">
              <p className="float-right-label">({this.props.size} {this.props.shoeSize})</p>
              <p className="float-left-label">
                Last Service: {new Date(this.props.lastService).toLocaleString()}<br />
                Next Service: {new Date(this.props.nextService).toLocaleString()}

              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Label;
