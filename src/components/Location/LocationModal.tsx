import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import Input from '../Inputs/Input';

// Axios for HTTP requests
import axios from 'axios';


class LocationModal extends React.Component<any, any>  {
    constructor(props: any){
        super(props);
        this.state = {
            location:{},
            message: ""
        };
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createLocation = this.createLocation.bind(this);
       }


       public createLocation(): void{
        axios.post(Config.server + '/location/', this.state.location).then( 
        response => {
        this.setState({message: "Succesfully created new location"});
        this.props.update();

        }).catch((error)=>{
        this.setState({message: "An error occured:" + error});
        console.log(error);
        });
    }

    public close() {

        this.props.close();
    }

    // Handle change of value events
    public handleChange(event) {
        // Make a copy of the object in the state
        const updatedValue = Object.assign({}, this.state.location, {[event.target.name]: event.target.value});

        // Now we can update the state
        this.setState({location:updatedValue});
    }
  public render() {
    return (
        <div className="custom-modal" > 
            <div className="modal-content" onChange={this.handleChange}>
                <h2 className="box-center">Create new location</h2>
                <p className="text-center">{this.state.message}</p>
                <Input placeholder="Enter title of location" label="Title" labelName="title" name="title" />
                <button onClick={this.createLocation}>Create location</button>
                <button onClick={this.close}>Close</button>
            </div>
      </div>
    );
    
  }
}

export default LocationModal;
