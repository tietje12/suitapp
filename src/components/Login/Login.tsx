import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';


// Components
import LoadingAnimation from '../Animation/LoadingAnimation';


// Axios for HTTP requests
import axios from 'axios';


class Login extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {

      loading: false,
    };

    // Remember to bind "this." methods
    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);

  }



  // Login
  public login(event): void {
    event.preventDefault();
    
    axios.post(Config.server + '/user/login', this.state.data).then(
      response => {

        sessionStorage.setItem('user', JSON.stringify(this.state.data));
        sessionStorage.setItem('authenticated', "true");
        this.props.history.push({
          pathname: '/',
        })

      }).catch((error) => {
        console.log(error);
      });

  }

  // Handle all input changes and update the state
  public handleChange(event) {
    // Assign this state to a new object
    const data = { ...this.state.data }
    data[event.target.name] = event.target.value;

    // Update the state with new data
    this.setState({ data });
  }



  public render() {
    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  <form>
                    <h2 className="box-center">Welcome to the Esvagt Suit System</h2>
                    <h4 className="text-center">Please login</h4>
                    <p className="input-label">Username</p>
                    <input onChange={this.handleChange} name="username" placeholder="Enter username" />
                    <p className="input-label">Password</p>
                    <input type="password" onChange={this.handleChange} name="password" placeholder="Enter password" />
                    <button type="submit" className="button" onClick={this.login}>Login</button>

                    {this.state.loading &&
                      <LoadingAnimation />
                    }
                  </form>
                </div>
              </div>
            </div>



          </div>
        </div>
      </div>
    );
  }
}

export default Login;
