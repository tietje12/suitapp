import * as React from 'react';
import { Redirect, Route,Switch } from 'react-router-dom';
import '../styles/App.css';


// Components
import BulkCheckoutFlow from './BulkCheckout/BulkCheckoutFlow';
import BulkCheckoutVests from './BulkCheckout/BulkeCheckOutVests';
import Checklist from './Checklist/Checklist';
import CreateChecklist from './Checklist/CreateChecklist';
import SingleChecklist from './Checklist/SingleChecklist';
import UpdateChecklist from './Checklist/UpdateChecklist';
import Dropoff from './Dropoff/Dropoff';
import SearchEmployee from './Employees/SearchEmployee';
import AllGuestUsers from './GuestUser/AllGuestUsers';
import CreateGuestUser from './GuestUser/CreateGuestUser';
import EditGuestUser from './GuestUser/EditGuestUser';
import SingleGuestUser from './GuestUser/SingleGuestUser';
import Login from './Login/Login';
import Result from './Result';
import Service from './Service';
import AllServicesSuit from './Service/AllServicesSuit';
import ControlForm from './Service/ControlForm';
import SingleServiceSuit from './Service/SingleServicSuit';
import AllSuits from './Suits/AllSuits';
import AssignSuit from './Suits/AssignSuit';
import AssignSuitEmployee from './Suits/AssignSuitEmployee';
import AssignSuitGuest from './Suits/AssignSuitGuest';
import AssignSuitVessel from './Suits/AssignSuitVessel';
import AvailableSuits from './Suits/AvailableSuits';
import CheckoutSuit from './Suits/CheckoutSuit';
import CreateSuit from './Suits/CreateSuit';
import DiscardSuit from './Suits/DiscardSuit';
import EditSuit from './Suits/EditSuit';
import LabelSuit from './Suits/LabelSuit';
import SearchSuit from './Suits/SearchSuit';
import CreateSuitType from './SuitType/CreateSuitType';
import SingleSuitType from './SuitType/SingleSuitType';
import SuitTypes from './SuitType/SuitTypes';
import CreateTask from './Tasks/CreateTask';
import EditTask from './Tasks/EditTask';
import SingleTask from './Tasks/SingleTask';
import Task from './Tasks/Task';
 

class ProtectedRoute extends React.Component<any, any>  {
  public requireAuth() {
    const authenticated = sessionStorage.getItem('authenticated');
    if (authenticated !== "true") {
      return false;  
    } else {
      return true;
    }
   
  }
  

 public render() {
    const { component: Component, ...props } = this.props

    


    return (
      <Route 
        {...props} 
        render={props => (
          this.requireAuth() ?
            <Component {...props} /> :
            <Redirect to='/login' />
        )} 
      />
    )
  }
}

class Main extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "", 
      edit: false,
      data: null   
    };

  }
  public requireAuth(props) {
    const authenticated = sessionStorage.getItem('authenticated');
    console.log(props)
    if (authenticated !== "true") {
      return false;  
    } else {
      return true;
    }
   
  }
  

  public render() {
    return (
      <div className="App" > 
        <Switch>
            <ProtectedRoute exact={true} path='/' component={SearchSuit} />
            <ProtectedRoute path='/createSuit' component={CreateSuit}  />
            <ProtectedRoute exact={true} path='/types' component={SuitTypes} />
            <ProtectedRoute path='/types/:id' component={SingleSuitType} />
            <ProtectedRoute exact={true} path='/checklist/:id' component={SingleChecklist} />
            <ProtectedRoute exact={true} path='/checklist/:id/edit' component={UpdateChecklist} />
            <ProtectedRoute exact={true} path='/service/:id' component={Service} />
            <ProtectedRoute exact={true} path='/service/:id/:controlfForm' component={ControlForm} />
            <ProtectedRoute path='/suit/:id/checkout' component={CheckoutSuit} />
            <ProtectedRoute path='/suit/:id/edit' component={EditSuit} />
            <ProtectedRoute path='/suit/:id/discard' component={DiscardSuit} />
            <ProtectedRoute path='/suit/:id/assign/employee' component={AssignSuitEmployee} />
            <ProtectedRoute path='/suit/:id/assign/guest' component={AssignSuitGuest} />
            <ProtectedRoute path='/suit/:id/assign/vessel' component={AssignSuitVessel} />
            <ProtectedRoute path='/suit/:id/assign' component={AssignSuit} />
            <ProtectedRoute path='/suit/:id/label' component={LabelSuit} />
            <ProtectedRoute path='/suit/:id/services' component={AllServicesSuit} />
            <ProtectedRoute path='/suit/:id/service/:id' component={SingleServiceSuit} />
            <ProtectedRoute path='/createSuitType' component={CreateSuitType} />
            <ProtectedRoute exact={true} path='/suits' component={AllSuits} />
            <ProtectedRoute exact={true} path='/available' component={AvailableSuits} />
            <ProtectedRoute path='/tasks' component={Task} />
            <ProtectedRoute exact={true} path='/task/:id' component={SingleTask} />
            <ProtectedRoute exact={true} path='/task/:id/edit' component={EditTask} />
            <ProtectedRoute path='/createTask' component={CreateTask} />
            <ProtectedRoute path='/checklist' component={Checklist} />
            <ProtectedRoute path='/createChecklist' component={CreateChecklist} />
            <ProtectedRoute path='/result' component={Result} />
            <ProtectedRoute path='/guestUser/create' component={CreateGuestUser} />
            <ProtectedRoute path='/guestUser/:id' component={SingleGuestUser} />
            <ProtectedRoute path='/guestUsers/:id/edit' component={EditGuestUser} />
            <ProtectedRoute path='/guestUsers' component={AllGuestUsers} />
            <ProtectedRoute path='/bulkCheckout' component={BulkCheckoutFlow} />
            <ProtectedRoute path='/bulkCheckoutVests' component={BulkCheckoutVests} />
            <ProtectedRoute path='/searchEmployees' component={SearchEmployee} />
            <Route path='/login' component={Login}/>
            <Route path='/dropoff' component={Dropoff}/>
        </Switch>
      </div>
    );
  }
}

export default Main;