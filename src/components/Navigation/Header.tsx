import * as React from 'react';
import { Link } from 'react-router-dom';
import '../../styles/App.css';



class Header extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = { value: '' };
    }
    // Removed visibility due to user mishandling
    // <li className="nav-item "><Link to='/tasks'>Tasks</Link></li>
    // <li className="nav-item "><Link to='/types'>Suit Types</Link></li>
    // <li className="nav-item "><Link to='/checklist'>Checklists</Link></li>
    public render() {

        return (
            <div className="header">
                <div className="container">
                    <nav className="navbar navbar-expand-md navbar-light">
                        <div className="collapse navbar-collapse d-flex justify-content-center" id="navbarsExampleDefault">
                            <ul className="navbar-nav ">
                                <li className="nav-item "><Link to='/'>Find suit</Link></li>
                                <li className="nav-item "><Link to='/available'>Available suits</Link></li>
                                <li className="nav-item "><Link to='/suits'>All Suits</Link></li>
                                <li className="nav-item "><Link to='/guestUsers'>Guest Users</Link></li>
                                <li className="nav-item "><Link to='/bulkcheckout'>Bulk Checkout</Link></li>
                                <li className="nav-item "><Link to='/bulkCheckoutVests'>Bulk Checkout Vests</Link></li>
                                <li className="nav-item "><Link to='/searchEmployees'>Search Employees</Link></li>
                            </ul>
                        </div>
                    </nav>

                </div>
            </div>
        );
    }
}

export default Header;
