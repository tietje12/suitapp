import * as React from 'react';
import '../../styles/App.css';



class ActionNotification extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {
      value: "",
    };
    this.close = this.close.bind(this);

  }

  public close() {
    this.props.close();
  }

  public render() {
    return (
      <div className="custom-modal" >
        <div className="modal-content">
          <h2 className="box-center">{this.props.title}</h2>
          <p className="text-center">{this.props.message}</p>
          <button className="box-center" onClick={this.close}>Ok</button>
        </div>
      </div>
    );
  }
}

export default ActionNotification;
