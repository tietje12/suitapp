import * as React from 'react';
import '../../styles/App.css';



class ActionNotificationWithFunction extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {
      value: "",
    };
    this.close = this.close.bind(this);
    this.action = this.action.bind(this);

  }

  public close() {
    this.props.close();
  }
  public action() {
    this.props.action();
  }
  public render() {
    return (
      <div className="custom-modal" >
        <div className="modal-content">
          <h2 className="box-center">{this.props.title}</h2>
          <p className="text-center">{this.props.message}</p>
          <button className="box-center" onClick={this.action}>Continue</button>
          <button className="box-center" onClick={this.close}>Cancel</button>
          
        </div>
      </div>
    );
  }
}

export default ActionNotificationWithFunction;
