import * as React from 'react';
import '../../styles/App.css';



class WarningNotification extends React.Component<any, any>  {

    constructor(props: any) {
        super(props);
        this.state = {
            value: "",
        };
        this.close = this.close.bind(this);

    }

    public close() {
        this.props.close();
    }

    public render() {
        return (
            <div className="warning-container" style={{backgroundColor: `#${this.props.color}`}} >
                <div className="close-warning" onClick={this.close}>X</div>
                <h3>{this.props.title}</h3>
                <p>{this.props.message}</p>


            </div>
        );
    }
}

export default WarningNotification;
