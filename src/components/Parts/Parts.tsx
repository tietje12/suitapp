
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'



import { library } from '@fortawesome/fontawesome-svg-core'
import * as React from 'react';

import '../../styles/App.css';

library.add(faTimesCircle)

class Parts extends React.Component<any, any>  {
    constructor(props: any){
        super(props);
        this.state = {
            value: ''
        
        };
        this.removePart = this.removePart.bind(this);
    
    }


public removePart(){
    this.props.action(this.props.index);
}


  public render() {
    return (
        <div>
            <li key={this.props.index}>
                {this.props.item} <span className="single-part" onClick={this.removePart}><FontAwesomeIcon icon="times-circle" /></span>
            </li>
      </div>
    );
    
  }
}

export default Parts;
