import * as React from 'react';
import { Link } from 'react-router-dom';

import jsPDF from 'jspdf';

import { default as Config } from '../config/config'

import html2canvas from 'html2canvas';
import '../styles/App.css';

import Label from './Label/Label';

// Axios for HTTP requests
import axios from 'axios';
import * as moment from 'moment';
import ActionNotification from './Notifications/ActionNotification';
import WarningNotification from './Notifications/WarningNotification';


class Result extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = {
      value: '',
      suit: {
        nextService: '',
        assignedTo: ''
      },
      serviceNextMonth: false,
    };
    this.closeWarning = this.closeWarning.bind(this);
    this.renderPDF = this.renderPDF.bind(this);
    this.setNextService = this.setNextService.bind(this);
    this.closeNotification = this.closeNotification.bind(this);
    
  }
  public componentDidMount() {
    this.getSuit(this.props.location.state.id);

  }

  // Close modale
  public closeWarning() {
    this.setState({
      hideWarning: true
    });
  }


  // Close modale
  public closeNotification() {
    this.setState({
      showModal: false
    });
    // Redirect to new checklist
    this.props.history.push({
      pathname: '/result',
      state: { id: this.state.suit.id }
    })
  }


  public getSuit(id) {
    if (id) {
      return axios.get(Config.server + '/suit/' + id)
        .then(
          response => {
            this.setState({ suit: response.data });
            this.getSuitType(this.state.suit.suitType);
            this.getLocation(this.state.suit.location);
            this.getDecomissioningReason(this.state.suit.decomissioningReason);
            if (this.state.suit.assignedTo && !this.state.suit.assignedTo[0].guest) {
              this.setState({
                employeeEndComing: moment(this.state.suit.assignedTo[0].EmployeeEmployedTo).isBetween(moment(), moment().add(90, 'd')),
              })
            }
            this.setState({
              serviceNextMonth: moment(this.state.suit.nextService).isBetween(moment(), moment().add(30, 'd')),
            })
          }
        )
    } else {
      return null;
    }
  }

  public getDecomissioningReason(id) {
    if (id) {
      return axios.get(Config.server + '/decomission/reasons/' + id)
        .then(
          response => {
            this.setState({ decomissioningReason: response.data.title });
          }
        )
    } else {
      return null;
    }
  }


  public setNextService() {
    if (this.state.suit.assignedTo[0].Employee) {
      console.log("Set new service date");
      axios.post(Config.server + '/ocs/nextService', { value: this.state.suit.assignedTo[0].EmployeeID })
        .then(
          response => {
            // Transform response into JSON
            const firstKey = Object.keys(response.data[0])[0];
            const employee = JSON.parse(response.data[0][firstKey]);
            const nextServiceDate = employee.Employee[0].NextActivityStart;
            console.log(nextServiceDate)
            console.log(moment(nextServiceDate).isValid())
            // Update next service  
            axios.post(Config.server + '/suit/' + this.state.suit.id + "/updateNextService", { newNextService: moment(nextServiceDate), user: this.state.suit.assignedTo[0], suit:this.state.suit}).then(
              response => {

                this.setState({
                  showModal: true,
                  modalMessage:"Next service date has been updated. Please close to proceed.",
                  modalTitle: "Next service date updated"
                })
                this.getSuit(this.props.location.state.id);
              }).catch((error) => {
                console.log(error);
              });
          }
        )



    } else {
      this.setState({
        showModal: true,
        modalMessage:"Cannot set next service date because no employee has been assigned to suit.",
        modalTitle: "No employee assigned"
      })
    }
  }

  public getSuitType(id) {
    if (id) {
      return axios.get(Config.server + '/suitType/' + id)
        .then(
          response => {
            this.setState({ suitType: response.data.title });
          }
        )
    } else {
      return null;
    }
  }


  public getLocation(id) {
    if (id) {
      return axios.get(Config.server + '/location/' + id)
        .then(
          response => {
            this.setState({ location: response.data.title });
          }
        )
    } else {
      return null;
    }
  }


  public renderPDF() {
    const input = document.getElementById('print');
    html2canvas(input, { scale: 2.2 })
      .then((canvas) => {
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF("p", "cm", [1016, 634]);
        pdf.addImage(imgData, 'PNG', 0, 0);
        pdf.save("download.pdf");
      })
      ;
  }

  public render() {
    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  {this.state.suit.assignedTo && this.state.suit.assignedTo[0].EmployeeCurrentlyEmployed === 0 && !this.state.hideWarning &&
                    <WarningNotification color="dc3545" title="Warning! Employee is not currently employed." message="Please be aware that the suit is assigned to an employee who is not currently employed." close={this.closeWarning} />
                  }

                  {this.state.serviceNextMonth && !this.state.hideWarning &&
                    <WarningNotification color="dc3545" title="Attention! Service is coming up." message="Please be aware that the suit has to be serviced within the next 30 days." close={this.closeWarning} />
                  }

                  {this.state.employeeEndComing && !this.state.hideWarning &&
                    <WarningNotification color="ffc107" title="Attention! Employee end date is coming up." message="Please be aware that the employee will have his last day within the next 90 days." close={this.closeWarning} />
                  }
                  <h2>{this.state.suit.type} suit information</h2>
                  <div className="row">
                    <div className="col-md-4">
                      <p className="input-label">ID</p>
                      <p>{this.state.suit.id}</p>
                      <p className="input-label">Created</p>
                      <p>{new Date(this.state.suit.createdAt).toLocaleDateString()}</p>
                      <p className="input-label">Updated</p>
                      <p>{new Date(this.state.suit.updatedAt).toLocaleDateString()}</p>
                      <p className="input-label">Location</p>
                      <p>{this.state.location}</p>
                      <p className="input-label">Comment</p>
                      <p>{this.state.suit.comment}</p>
                    </div>
                    <div className="col-md-4">
                      <p className="input-label">Assigned to</p>
                      {this.state.suit.assignedTo && !this.state.suit.assignedTo[0].guest && !this.state.suit.assignedTo[0].vessel &&
                        <p style={{ color: this.state.suit.assignedTo && this.state.suit.assignedTo[0].EmployeeCurrentlyEmployed === 0 ? "red" : "#575858" }}>{this.state.suit.assignedTo[0].EmployeeID} | {this.state.suit.assignedTo[0].EmployeeName}</p>
                      }
                      {this.state.suit.assignedTo && this.state.suit.assignedTo[0].guest &&
                        <Link to={`/guestUser/${this.state.suit.assignedTo[0].id}`}>
                          <a href="">Guest: {this.state.suit.assignedTo[0].id} | {this.state.suit.assignedTo[0].name}</a>
                        </Link>
                      }
                      {!this.state.suit.assignedTo &&
                        <p>Unassigned</p>
                      }

                      {this.state.suit.assignedTo && this.state.suit.assignedTo[0].vessel &&
                        <p>Vessel: {this.state.suit.assignedTo[0].name}</p>
                      }
                      <p className="input-label">Employee End Date</p>
                      {this.state.suit.assignedTo && !this.state.suit.assignedTo[0].guest && !this.state.suit.assignedTo[0].vessel &&
                        <p style={{ color: this.state.employeeEndComing ? "#ffc107" : "#575858" }}>{new Date(this.state.suit.assignedTo[0].EmployeeEmployedTo).toLocaleDateString()}</p>
                      }
                      {this.state.suit.assignedTo && this.state.suit.assignedTo[0].guest &&
                        <p>Guest User</p>
                      }
                      {this.state.suit.assignedTo && this.state.suit.assignedTo[0].vessel &&
                        <p>Assigned to vessel</p>
                      }
                      {this.state.suit.status &&
                        <p className="input-label">Status</p>
                      }
                      {this.state.suit.status &&

                        <p>{this.state.suit.status}{this.state.decomissioningReason ? <span>: {this.state.decomissioningReason}</span> : ''}</p>
                      }
                      <p className="input-label">Suit Type</p>
                      <p>{this.state.suitType}</p>
                      <p className="input-label">Vessel</p>
                      <p>{this.state.suit.vessel}</p>

                      <p className="input-label">Checkout Comment</p>
                      <p>{this.state.suit.checkOutComment}</p>
                    </div>
                    <div className="col-md-4">
                      <p className="input-label">Size</p>
                      <p>{this.state.suit.size}</p>
                      <p className="input-label">Next service</p>
                      <p style={{ color: this.state.serviceNextMonth ? "red" : "#575858" }}>{new Date(this.state.suit.nextService).toLocaleDateString()}</p>
                      <p className="input-label">Last service</p>
                      <p>{new Date(this.state.suit.lastService).toLocaleDateString()}</p>
                      {this.state.suit.inUseTempComment &&
                        <p className="input-label">IN-USE TEMP COMMENT</p>
                      }
                      {this.state.suit.inUseTempComment &&
                        <p>{this.state.suit.inUseTempComment}</p>
                      }
                    </div>

                  </div>
                  <Link to={`/suit/${this.state.suit.id}/assign`}>
                    <button className="button">Assign</button>
                  </Link>
                  <Link to={`/suit/${this.state.suit.id}/edit`}>
                    <button className="button">Edit</button>
                  </Link>
                  <Link to={`/suit/${this.state.suit.id}/discard`}>
                    <button className="button">Discard</button>
                  </Link>
                  <Link to={`/service/${this.state.suit.id}`}>
                    <button className="button">Perform Service</button>
                  </Link>
                  <Link to={`/suit/${this.state.suit.id}/services`}>
                    <button className="button">Service Overview</button>
                  </Link>
                  <Link to={`/suit/${this.state.suit.id}/checkout`}>
                    <button className="button">Checkout</button>
                  </Link>
                  <button className="button" onClick={this.setNextService}>Next Service</button>
                  <hr />
                  <h2>Label information</h2>
                  <button className="box-center" onClick={this.renderPDF}>Print Label</button>

                  {this.state.suit.assignedTo && this.state.suit.assignedTo[0].vessel &&
                    <Label barcode={this.state.suit.id} vessel={this.state.suit.vessel} id={this.state.suit.id} size={this.state.suit.size} shoeSize={this.state.suit.shoeSize} type={this.state.suitType} assignedTo=" " assignedToNumber={this.state.suit.assignedTo[0].name} nextService={this.state.suit.nextService} lastService={this.state.suit.lastService} />
                  }

                  {this.state.suit.assignedTo && !this.state.suit.assignedTo[0].vessel &&
                    <Label barcode={this.state.suit.id} vessel={this.state.suit.vessel} id={this.state.suit.id} size={this.state.suit.size} shoeSize={this.state.suit.shoeSize} type={this.state.suitType} assignedTo={this.state.suit.assignedTo[0].EmployeeName || this.state.suit.assignedTo[0].name} assignedToNumber={this.state.suit.assignedTo[0].EmployeeID || this.state.suit.assignedTo[0].id} nextService={this.state.suit.nextService} lastService={this.state.suit.lastService} />
                  }

                  {!this.state.suit.assignedTo &&
                    <Label barcode={this.state.suit.id} vessel={this.state.suit.vessel} id={this.state.suit.id} size={this.state.suit.size} shoeSize={this.state.suit.shoeSize} type={this.state.suitType} assignedTo="Unassigned" assignedToNumber="" nextService={this.state.suit.nextService} lastService={this.state.suit.lastService} />
                  }

                  {this.state.showModal &&
                    <ActionNotification title={this.state.modalTitle} message={this.state.modalMessage} close={this.closeNotification} />
                  }

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );

  }
}

export default Result;
