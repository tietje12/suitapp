import * as React from 'react';
import {default as Config} from '../config/config'

import '../styles/App.css';
import LoadingAnimation from './Animation/LoadingAnimation';

// Axios for HTTP requests
import axios from 'axios';

import { Link } from 'react-router-dom';

class Service extends React.Component<any, any>  {

    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
                assignedTo: {
                    EmployeeId: "",
                    First: "",
                    Last: ""
                }
            },
            loading: true,
            checklist: []
        };
    }

    public componentDidMount() {
        this.getSuit();
    }

    // Map tasks
    public mapForms(forms) {
        if (!forms) {
            return null;
        }

        return (
            <div className="row">
                {forms.map((form, i) => {
                    return <div key={i} className="col-md-4">
                        <Link to={this.props.location.pathname + `/${form._id}`}>
                            <button>{form.title}</button>
                        </Link>
                    </div>
                })}
            </div>
        );
    }

    // Get Suit
    public getSuit(): void {
        axios.get(Config.server + '/suit/' + this.props.match.params.id).then(
            response => {
                this.setState({
                    data: response.data,
                })
                this.getChecklist(response.data.suitType)
            }).catch((error) => {
                console.log(error);
            });
    }

    // Get checklist
    public getChecklist(id): void {
        axios.get(Config.server + '/checklist/suittype/' + id).then(
            response => {
                this.setState({
                    checklist: response.data,
                    loading: false

                });
            }).catch((error) => {
                console.log(error);
            });
    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Please select control form</h2>
                                    {this.mapForms(this.state.checklist)}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                {this.state.loading &&
                    <LoadingAnimation />
                  }
                                    <Link to={{
                                        pathname: '/result',
                                        state: { id: this.state.data.id }
                                    }}>
                                        <button className="button">Cancel</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Service;