import * as React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import { default as Config } from '../../config/config'


import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';

class AllServicesSuit extends React.Component<any, any>  {

    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
            },
            checklist:
            {
                tasks: []
            },
            service: {
                tasks: []
            },
            suitType: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.getChecklists = this.getChecklists.bind(this);
        this.checklistTypeFormatter = this.checklistTypeFormatter.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    public componentDidMount() {
        this.getChecklists();
        this.getServicePerformed();
        this.getSuit();
        
    }
    


    public onClick(e, row, rowIndex) {
        if (!row.id) {
            return;
        }

        this.props.history.push({
            pathname: '/suit/'+this.state.data._id+'/service/' + row._id,
            state: { id: row.checklistId, suit: this.state.data }
        });
    }

    // Close modale
    public closeNotification() {
        this.setState({
            success: false
        });
        // Redirect to new checklist
        // this.props.history.push("/checklist/" + this.state.resultResponse.data._id);
        this.props.history.push({
            pathname: '/result',
            state: { id: this.state.data.id }
        })
    }
    // Get Suit
    public getSuit(): void {
        axios.get(Config.server + '/suit/' + this.props.match.params.id).then(
            response => {
                console.log(response);
                this.setState({
                    data: response.data,
                })
                


            }).catch((error) => {
                console.log(error);
            });
    }

    // Get all checklists
    public getChecklists(): void {
        axios.get(Config.server + '/checklist').then(
            response => {
                console.log(response);
                this.setState({
                    checklists: response.data,
                })
                
            }).catch((error) => {
                console.log(error);
            });
    }



    // Get service performed
    public getServicePerformed(): void {
        axios.get(Config.server + '/servicePerformed/' + this.props.match.params.id + '/all').then(
            response => {
                console.log(response);
                this.setState({
                    services: response.data,
                })
                
            }).catch((error) => {
                console.log(error);
            });
    }


    // Handle change of value events
    public handleChange(event) {
        console.log(event.target.value)
        // We need to check if the input is a task check or if it the performedBy input
        let performedBy;
        if (event.target.name === "performedBy") {
            performedBy = event.target.value;
        }

        // Take the task list and assign it to new array
        // If the input name matches a task id assign new "value" and "remark"
        const tasks = this.state.checklist.tasks;
        for (const task of tasks) {
            if (event.target.name === task._id) {
                task.value = event.target.value
            } else if (event.target.name === task._id + "-remark") {
                task.remark = event.target.value
            }
        }
        // Make a copy of the object in the state
        const updatedValue = Object.assign({}, this.state.service, { servicePerformedBy: performedBy, tasks });

        // Now we can update the state
        this.setState({ service: updatedValue });

    }

    public checklistTypeFormatter(cell, row) {
        const checklist = this.state.checklists.find(x => x._id === cell);
        if (checklist) {
            return checklist.title;
        }

        return "";
    }

    public dateFormatter(cell, row) {
        return new Date(cell).toLocaleString()
    }

    public render() {

        const columns = [
            {
                dataField: 'createdAt',
                text: 'Date',
                formatter: this.dateFormatter,
                sort: true,
                filter: textFilter()
                
              },
              {
                dataField: 'performedBy',
                text: 'Performed By',
                sort: true,
                filter: textFilter()
              },
              {
                dataField: 'checklistId',
                text: 'Service Type',
                formatter: this.checklistTypeFormatter,
                filter: textFilter(),
                filterValue: this.checklistTypeFormatter,
                sort: true
              },
              
            
        ];

        const defaultSort = [{
            dataField: 'id',
            order: 'asc'
        }];

        const rowEvents = {
            onClick: this.onClick
        };


        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Service history</h2>
                                    {this.state.services && this.state.checklists && this.state.services.length ?
                                        <BootstrapTable keyField='_id' rowEvents={rowEvents} data={this.state.services} bootstrap4={true} columns={columns} striped={true} condensed={true} hover={true} bordered={false} defaultSorted={defaultSort} filter={filterFactory()} noDataIndication="No services performed." />
                                        : ''}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default AllServicesSuit;
