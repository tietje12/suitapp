import * as React from 'react';
import { default as Config } from '../../config/config'

import '../../styles/App.css';

import Input from '../Inputs/Input';

// Axios for HTTP requests
import axios from 'axios';
import ActionNotification from '../Notifications/ActionNotification';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

import { SketchField, Tools } from 'react-sketch';

class ControlForm extends React.Component<any, any>  {
    private sketch;
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
            },
            checklist:
            {
                tasks: []
            },
            service: {
                tasks: []
            },
            suitType: "",
            user: {
                username: ""
            }

        };
        this.handleChange = this.handleChange.bind(this);
        this.performService = this.performService.bind(this);
        this.renderEditable = this.renderEditable.bind(this);
        this.renderEditableDropdown = this.renderEditableDropdown.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.setAllToOk = this.setAllToOk.bind(this);
    }

    public componentDidMount() {
        // Get Currently loggein user
        this.setState({
            user: JSON.parse(sessionStorage.getItem('user') || '{}')
        })

        this.getSuit();
        this.getChecklist();

    }
    // Close modale
    public closeNotification() {
        this.setState({
            success: false
        });
        // Redirect to new checklist
        // this.props.history.push("/checklist/" + this.state.resultResponse.data._id);
        this.props.history.push({
            pathname: '/result',
            state: { id: this.state.data.id }
        })
    }
    public renderEditable(cellInfo) {
        return (
            <div className="input"
                style={{ backgroundColor: "#ffffff", border: "2px solid #c9d0e1", borderRadius: "5px" }}
                contentEditable={true}
                suppressContentEditableWarning={true}
                onBlur={e => {
                    const value = [...this.state.checklist.tasks];
                    value[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
                    this.setState({ value });
                }}
                dangerouslySetInnerHTML={{
                    __html: this.state.checklist.tasks[cellInfo.index][cellInfo.column.id]
                }}
            />
        );
    }

    public renderEditableDropdown(cellInfo) {
        return (
            <select className="task-select"
                contentEditable={true}
                suppressContentEditableWarning={true}
                onBlur={e => {
                    const value = [...this.state.checklist.tasks];
                    value[cellInfo.index][cellInfo.column.id] = e.target.value;
                    this.setState({ value });
                }}
            >
                <option selected={true} disabled={true}>- Select -</option>
                <option value="R">Reparing</option>
                <option value="C">Changed</option>
                <option value="X">Ok</option>
                <option value="/">Not Needed</option>
            </select>
        );
    }



    // Set all tasks to OK
    public setAllToOk() {
        const list = document.getElementsByClassName("task-select");
        console.log(list);


        for (const item of list as any) {
            (item as HTMLInputElement).value = "X"
        }


        const value = [...this.state.checklist.tasks];
        const tasks = this.state.checklist.tasks
        for (let i = 0; i < tasks.length; i++) {
            value[i].check = "x";

        }
        this.setState({ value });
    }

    // Create service-performed entry
    public performService() {
        console.log("performing service")
        let performedByValue = this.state.service.servicePerformedBy;
        // If performedBy hasn't been set use the current user from session storage
        if (!performedByValue) {
            performedByValue = this.state.user.username;
        }
        let imageObj;

        if (this.state.checklist.image) {
            imageObj = this.sketch.toDataURL();
        } else {
            imageObj = "";
        }


        const serviceObject = {
            suitId: this.state.data._id,
            id: this.state.data.id,
            checklistId: this.state.checklist._id,
            performedBy: performedByValue,
            tasks: this.state.checklist.tasks,
            image: imageObj,
            suitSnapShot: this.state.data
        }

        axios.post(Config.server + '/servicePerformed/', serviceObject).then(
            response => {
                console.log("Service completed");
                console.log(response);
                this.updateSuit();
            }).catch((error) => {
                this.setState({
                    success: true,
                    resultResponse: error,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });
    }

    public updateSuit() {
        // Set lastService date to current date
        const updatedSuit = Object.assign({}, this.state.data);
        updatedSuit.lastService = Date.now();

        axios.post(Config.server + '/suit/' + this.state.data.id, updatedSuit).then(
            (response) => {
                this.setState({
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "Service has been completed."
                })
            }).catch((error) => {
                this.setState({
                    success: true,
                    resultResponse: error,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });
    }

    // Get Suit
    public getSuit(): void {
        axios.get(Config.server + '/suit/' + this.props.match.params.id).then(
            response => {
                console.log(response);


                this.setState({
                    data: response.data,
                })
                this.getSuitType(this.state.data.suitType);
                this.getLocation(this.state.data.location);


            }).catch((error) => {
                console.log(error);
            });
    }

    // Get checklist
    public getChecklist(): void {
        axios.get(Config.server + '/checklist/' + this.props.match.params.controlfForm).then(
            response => {

                // Replace (undefined) in task label
                const array = response.data.tasks
                array.forEach(element => {
                    element.label = element.label.replace("(undefined)", "")
                });

                response.data.tasks = array;

                this.setState({
                    checklist: response.data,
                })
                if (response.data.image) {
                    this.sketch.setBackgroundFromDataUrl("data:image/gif;base64," + response.data.image)
                }
            }).catch((error) => {
                console.log(error);
            });
    }
    // Get suit type
    public getSuitType(id) {
        if (id) {
            return axios.get(Config.server + '/suitType/' + id)
                .then(
                    response => {
                        this.setState({ suitType: response.data.title });
                    }
                )
        } else {
            return null;
        }
    }

    // Get location
    public getLocation(id) {
        if (id) {
            return axios.get(Config.server + '/location/' + id)
                .then(
                    response => {
                        this.setState({ location: response.data.title });
                    }
                )
        } else {
            return null;
        }
    }

    // Handle change of value events
    public handleChange(event) {
        console.log(event)
        // We need to check if the input is a task check or if it the performedBy input
        let performedBy;
        if (event.target.name === "performedBy") {
            performedBy = event.target.value;
        }

        // Take the task list and assign it to new array
        // If the input name matches a task id assign new "value" and "remark"
        const tasks = this.state.checklist.tasks;
        for (const task of tasks) {
            if (event.target.name === task._id) {
                task.value = event.target.value
            } else if (event.target.name === task._id + "-remark") {
                task.remark = event.target.value
            }
        }
        // Make a copy of the object in the state
        const updatedValue = Object.assign({}, this.state.service, { servicePerformedBy: performedBy, tasks });

        // Now we can update the state
        this.setState({ service: updatedValue });

    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Suit information</h2>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <p className="input-label">ID</p>
                                            <p>{this.state.data.id}</p>
                                            <p className="input-label">Created</p>
                                            <p>{new Date(this.state.data.createdAt).toLocaleString()}</p>

                                            <p className="input-label">Location</p>
                                            <p>{this.state.location}</p>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Assigned to</p>
                                            {this.state.data.assignedTo && !this.state.data.assignedTo[0].guest &&
                                                <p>{this.state.data.assignedTo[0].EmployeeID} | {this.state.data.assignedTo[0].EmployeeName}</p>
                                            }
                                            {this.state.data.assignedTo && this.state.data.assignedTo[0].guest &&
                                                <p>Guest: {this.state.data.assignedTo[0].id} | {this.state.data.assignedTo[0].name}</p>
                                            }
                                            {!this.state.data.assignedTo &&
                                                <p>Unassigned</p>
                                            }
                                            <p className="input-label">Updated</p>
                                            <p>{new Date(this.state.data.updatedAt).toLocaleString()}</p>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Model</p>
                                            <p>{this.state.suitType}</p>
                                            <p className="input-label">Size</p>
                                            <p>{this.state.data.size}</p>
                                            <p className="input-label">Next service</p>
                                            <p>{new Date(this.state.data.nextService).toLocaleString()}</p>
                                        </div>
                                    </div>

                                    <div className="row" onChange={this.handleChange}>
                                        <div className="col-md-6">
                                            <Input placeholder={this.state.user.username} label="Service performed by" labelName="name" name="performedBy" />
                                        </div>
                                        <div className="col-md-12">
                                            <h2>Control Form: {this.state.checklist.title}</h2>
                                            <p>Following parts are repared/changed</p>
                                            <p>(Check with: R = Reparing, C = Changed, X = Ok, / = Not Needed)</p>
                                            <button className="button" onClick={this.setAllToOk} style={{ marginBottom: "10px" }}>Set all to OK</button>

                                            <ReactTable
                                                style={{ marginBottom: "10px" }}
                                                minRows="0"
                                                data={this.state.checklist.tasks}
                                                showPagination={false}
                                                columns={[
                                                    {
                                                        Header: "Task",
                                                        accessor: "label",
                                                        className: 'task-title',
                                                    },
                                                    {
                                                        Header: "Check",
                                                        accessor: "check",
                                                        Cell: this.renderEditableDropdown
                                                    },
                                                    {
                                                        Header: "Remarks",
                                                        accessor: "remarks",
                                                        Cell: this.renderEditable
                                                    },
                                                ]}
                                                className="-striped -highlight"
                                            />
                                        </div>

                                        <hr />
                                        {this.state.checklist.image &&
                                            <div className="col-md-12" id="sketch-container">
                                                <h2>Form for marking</h2>

                                                <SketchField width='1000px'
                                                    ref={(c) => this.sketch = c}
                                                    name="sketch"
                                                    height='800px'
                                                    tool={Tools.Circle}
                                                    lineColor='red'
                                                    lineWidth={2}
                                                />
                                            </div>
                                        }
                                        <div className="col-md-12">
                                            <button className="button" onClick={this.performService}>Complete control</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default ControlForm;
