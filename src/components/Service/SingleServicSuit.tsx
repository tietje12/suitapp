import * as React from 'react';
import { default as Config } from '../../config/config'

import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';
import ActionNotification from '../Notifications/ActionNotification';



class SingleServiceSuit extends React.Component<any, any>  {

    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
            },
            checklist:
            {
                tasks: []
            },
            service: {
                tasks: [],
                suitSnapShot: {}
            },
            suitType: "",
            user: {
                username: ""
            }

        };
        this.closeNotification = this.closeNotification.bind(this);
        this.print = this.print.bind(this);
    }

    public componentDidMount() {
        // Get Currently loggein user
        this.setState({
            user: JSON.parse(sessionStorage.getItem('user') || '{}')
        })

        this.getSuit();
        this.getServicePerformed();

    }
    // Close modale
    public closeNotification() {
        this.setState({
            success: false
        });
        // Redirect to new checklist
        // this.props.history.push("/checklist/" + this.state.resultResponse.data._id);
        this.props.history.push({
            pathname: '/result',
            state: { id: this.state.data.id }
        })
    }




    // Get service performed
    public getServicePerformed(): void {
        axios.get(Config.server + '/servicePerformed/' + this.props.match.params.id).then(
            response => {
                console.log(response);
                this.setState({
                    service: response.data,
                })
                this.getLocation(this.state.service.suitSnapShot.location);
            }).catch((error) => {
                console.log(error);
            });
    }


    // Get Suit
    public getSuit(): void {
        console.log(this.props.location.state.suit.id)
        axios.get(Config.server + '/suit/' + this.props.location.state.suit.id).then(
            response => {
                console.log(response);
                this.setState({
                    data: response.data,
                })
                this.getSuitType(this.state.data.suitType);
                


            }).catch((error) => {
                console.log(error);
            });
    }

    // Get location
    public getLocation(id) {
        if (id) {
          return axios.get(Config.server + '/location/' + id)
            .then(
              response => {
                this.setState({ location: response.data.title });
              }
            )
        } else {
          return null;
        }
      }


    public print() {
        window.print();
    }


    // Get suit type
    public getSuitType(id) {
        console.log(id);
        if (id) {
            return axios.get(Config.server + '/suitType/' + id)
                .then(
                    response => {
                        this.setState({ suitType: response.data.title });
                    }
                )
        } else {
            return null;
        }
    }



    public render() {
        if (!this.state.service || !this.state.service.suitSnapShot) {
            return null;
        }

        return (
            <div className="App service-print" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Suit information ({new Date(this.state.service.createdAt).toLocaleString()})</h2>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <p className="input-label">ID</p>
                                            <p>{this.state.service.suitSnapShot.id}</p>
                                            <p className="input-label">Created</p>
                                            <p>{new Date(this.state.service.suitSnapShot.createdAt).toLocaleString()}</p>

                                            <p className="input-label">Location</p>
                                            <p>{this.state.location}</p>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Assigned to</p>
                                            {this.state.service.suitSnapShot.assignedTo && !this.state.service.suitSnapShot.assignedTo[0].guest &&
                                                <p>{this.state.service.suitSnapShot.assignedTo[0].EmployeeID} | {this.state.service.suitSnapShot.assignedTo[0].EmployeeName}</p>
                                            }
                                            {this.state.service.suitSnapShot.assignedTo && this.state.service.suitSnapShot.assignedTo[0].guest &&
                                                <p>Guest: {this.state.service.suitSnapShot.assignedTo[0].id} | {this.state.service.suitSnapShot.assignedTo[0].name}</p>
                                            }
                                            {!this.state.data.assignedTo &&
                                                <p>Unassigned</p>
                                            }
                                            <p className="input-label">Updated</p>
                                            <p>{new Date(this.state.service.suitSnapShot.updatedAt).toLocaleString()}</p>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Suit Type</p>
                                            <p>{this.state.suitType}</p>
                                            <p className="input-label">Size</p>
                                            <p>{this.state.service.suitSnapShot.size}</p>
                                            <p className="input-label">Next service</p>
                                            <p>{new Date(this.state.service.suitSnapShot.nextService).toLocaleString()}</p>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-12">
                                            <h2>Control Form: {this.state.checklist.title}</h2>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Service performed by</p>

                                            <p>{this.state.service.performedBy}</p>
                                        </div>
                                        <div className="col-md-4">
                                            <p className="input-label">Service date</p>
                                            <p>{new Date(this.state.service.createdAt).toLocaleString()}</p>
                                        </div>
                                        <div className="col-md-12">
                                            <table className="table">
                                                <tr>
                                                    <th>Task</th>
                                                    <th className="text-center">Action</th>
                                                    <th className="text-center">Remark</th>
                                                </tr>
                                                {this.state.service.tasks.map((value, index) => (
                                                    <tr key={index}>
                                                        <td >{value.label}</td>
                                                        <td className="text-center">{value.check}</td>
                                                        <td className="text-center">{value.remarks}</td>
                                                    </tr>
                                                ))}
                                            </table>
                                        </div>

                                        <hr />
                                        {this.state.service.image &&
                                        <div className="col-md-12" id="sketch-container">
                                            <h2>Form for marking:</h2>
                                            <img src={`data:image/png;base64, ${this.state.service.image}`} />
                                            <button className="button" onClick={this.print}>Print</button>

                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default SingleServiceSuit;
