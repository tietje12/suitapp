import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Components
import Input from '../Inputs/Input';
import ActionNotification from '../Notifications/ActionNotification';

// Axios for HTTP requests
import axios from 'axios';

class CreateSuitType extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            parts: []
        };

        // Bind functions
        this.handleChange = this.handleChange.bind(this);
        this.createSuitType = this.createSuitType.bind(this);
        this.addParts = this.addParts.bind(this);
        this.removePart = this.removePart.bind(this);
        this.closeNotification = this.closeNotification.bind(this);

    }

    public addParts(item): void {
        this.setState(prevState => ({
            parts: [...prevState.parts, item]
        }))
    }

    public removePart(index) {
        const array = [...this.state.parts]; // make a separate copy of the array
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ parts: array });
        }
    }



    public createSuitType(): void {
        console.log(this.state.suit);
        const updatedValue = Object.assign({}, this.state.suit, { parts: this.state.parts });
        this.setState({ suit: updatedValue });
        axios.post(Config.server + '/suitType/', this.state.suit).then(
            response => {
                console.log(response);
                this.setState({
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "Suittype has been created."
                })
            }).catch((error) => {
                console.log(error);
                this.setState({
                    success: true,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });

    }
    public closeNotification() {
        this.setState({
            success: false
        });
       
                this.props.history.push({
                    pathname: '/types',
                })
          }
    // Handle change of value events
    public handleChange(event) {
        // Make a copy of the object in the state
        const updatedValue = Object.assign({}, this.state.suit, { [event.target.name]: event.target.value });

        // Now we can update the state
        this.setState({ suit: updatedValue });
    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-8">
                                <div className="box" onChange={this.handleChange}>
                                    <h2>Create new suit type</h2>
                                    <Input placeholder="Enter Title" label="Title" labelName="title" name="title" />
                                    <button onClick={this.createSuitType}>Create Suite</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );

    }
}

export default CreateSuitType;
