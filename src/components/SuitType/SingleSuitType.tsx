import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import ActionNotification from '../Notifications/ActionNotification';

// Axios for HTTP requests
import axios from 'axios';


class SingleSuitType extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = {
      value: '',
      data: {
        parts: []
      }
    };

    this.deleteSuitType = this.deleteSuitType.bind(this);
    this.closeNotification = this.closeNotification.bind(this);

  }
  public closeNotification() {
    this.setState({
      success: false
    });
    this.props.history.push({
      pathname: '/types'
    })
  }
  public componentWillMount() {
    this.getSuitTypes(this.props.match.params.id);
  }


  // Delete suit tyoe
  public deleteSuitType(): void {
    axios.delete(Config.server + '/suitType/' + this.props.match.params.id).then(
      response => {
        console.log(response);
        this.setState({
          success: true,
          resultResponse: response,
          resultTitle: "Success",
          resultMessage: "Suit type has been deleted. Please proceed by clicking on ok."
        })

      }).catch((error) => {
        console.log(error);
        this.setState({
          success: true,
          resultTitle: "Error",
          resultMessage: error
        })
      });
  }

  // Get single suit type
  public getSuitTypes(id): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/suitType/' + id)
      .then(
        response => {
          this.setState({
            data: response.data,
            loading: false
          })
        }
      )
  }


  public render() {
    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  <h2>Suit type: {this.state.data.title}</h2>
                  <button className="button" onClick={this.deleteSuitType}>Delete suit type</button>

                </div>
              </div>
            </div>



          </div>
        </div>
        {this.state.success &&
          <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
        }
      </div>
    );
  }
}

export default SingleSuitType;
