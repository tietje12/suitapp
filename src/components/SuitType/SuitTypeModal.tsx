import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import Input from '../Inputs/Input';

// Axios for HTTP requests
import axios from 'axios';


class SuitTypeModal extends React.Component<any, any>  {
    constructor(props: any){
        super(props);
        this.state = {
            suit:{},
            message: ""
        };
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.createSuitType = this.createSuitType.bind(this);
       }


       public createSuitType(): void{
        axios.post(Config.server + '/suitType/', this.state.suit).then( 
        response => {
        this.setState({message: "Succesfully created new suittype"});
        this.props.update();
        }).catch((error)=>{
        this.setState({message: "An error occured:" + error});
        console.log(error);
        });

    }

    public close() {
        this.props.close();
    }

    // Handle change of value events
    public handleChange(event) {
        // Make a copy of the object in the state
        const updatedValue = Object.assign({}, this.state.suit, {[event.target.name]: event.target.value});

        // Now we can update the state
        this.setState({suit:updatedValue});
    }
  public render() {
    return (
        <div className="custom-modal" > 
            <div className="modal-content" onChange={this.handleChange}>
                <h2 className="box-center">Create new suittype</h2>
                <p className="text-center">{this.state.message}</p>
                <Input placeholder="Enter suittype" label="Title" labelName="title" name="title" />
                <button onClick={this.createSuitType}>Create type</button>
                <button onClick={this.close}>Close</button>
            </div>
      </div>
    );
    
  }
}

export default SuitTypeModal;
