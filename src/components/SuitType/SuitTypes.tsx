import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';


// Axios for HTTP requests
import axios from 'axios';
import { Link } from 'react-router-dom';

class SuitTypes extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "",
      data: [],
      loading: false,   
    };


  }

  public componentDidMount() {
    this.getSuitTypes();
    }

  public handleChange(event) {
    console.log(event);
    this.setState({value: event.target.value});
  }


    // Get all suits
  public getSuitTypes(): void{
    this.setState({
      loading:true
    })
    axios.get(Config.server + '/suitType')
    .then(
        response =>{
          this.setState({
            data:response.data,
            loading: false
          })
        } 
      ) 
  }


  
  public render() {
    return (
        
      <div className="App" > 
        <div className="container">
          <div className="wrapper">
            <div className="row">
                  <div className="col-sm-12">
                      <div className="box">
                      <h2>Suit Types</h2>
                      <ul className="infoList">
                        {this.state.data.map((item, index) =>
                            <li key={index}>
                                <Link to={`/types/${item._id}`}>
                                    {item.title}
                                </Link>
                            </li>
                            

                        )}
                        </ul>
                        <Link to='/createSuitType'><button>Create new type</button></Link>
                      </div>
                  </div>
              </div>
          
          
          
          </div>
        </div>
      </div>
    );
  }
}

export default SuitTypes;
