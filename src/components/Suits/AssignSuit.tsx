import * as React from 'react';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

import axios from 'axios';

class AssignSuit extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
        };

        this.unassignSuit = this.unassignSuit.bind(this);

    }

    public componentWillMount() {
        this.getSuit();
    }

    public getSuit(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }

    public unassignSuit() {
        console.log(this.state.data)
        const fields = this.state.data;
        
        console.log(this.state.data)
        if (this.state.data.assignedTo && this.state.data.assignedTo !== "Unassigned") {
            console.log("Remove it from employee")
            const payload = {
                suitId: this.state.data.id,
                validTo: this.state.data.nextService
            }
            // Remove certificate from user
            axios.post(Config.server + '/cert/' + this.state.data.assignedTo[0].EmployeeID + '/removeCert', payload).then(
                (response) => {
                    console.log(response);
                }).catch((error) => {
                    console.log(error);
                });
        }
        fields.assignedTo = "";
        const updatedSuit = Object.assign({}, this.state.data, fields);
        axios.post(Config.server + '/suit/' + this.state.data.id, updatedSuit).then(
            () => {

                this.props.history.push({
                    pathname: '/result',
                    state: { id: this.state.data.id }
                })

            }).catch((error) => {
                console.log(error);
            });
    }

    public render() {
        if (!this.state.data.id) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Assign suit {this.state.data.id}</h2>

                                    <Link to={`/suit/${this.state.data.id}/assign/employee`}>
                                        <button className="button">Employee</button>
                                    </Link>
                                    <Link to={`/suit/${this.state.data.id}/assign/vessel`}>
                                        <button className="button">Vessel</button>
                                    </Link>

                                    <Link to={`/suit/${this.state.data.id}/assign/guest`}>
                                        <button className="button">Guest</button>
                                    </Link>

                                    <button className="button" onClick={this.unassignSuit}>Unassign</button>

                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <Link to={{
                                        pathname: '/result',
                                        state: { id: this.state.data.id }
                                    }}>
                                        <button className="button">Cancel</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AssignSuit;