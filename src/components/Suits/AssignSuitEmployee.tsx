import { FormValidation } from 'calidation';
import * as React from 'react';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'

import '../../styles/App.css';

import axios from 'axios';
// import { userInfo } from 'os';

class AssignSuitEmployee extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            allowNew: false,
            loading: false,
            employees: []
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.assignToEmployee = this.assignToEmployee.bind(this);
        this.handleSearch = this.handleSearch.bind(this);

        this.formConfig = {
            id: {
                isRequired: 'Employee is required'
            }
        }
    }

    public componentWillMount() {
        this.getSuit();
    }

    public getSuit(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }

    public handleSearch = (query) => {
        this.setState({ loading: true });

        axios.post(Config.server + '/ocs/', { "value": query }).then(response => {
            const firstKey = Object.keys(response.data[0]);
            const data = JSON.parse(response.data[0][firstKey[0]]);
            this.setState({
                employees: data.Employee,
                loading: false
            })
        }).catch((error) => {
            console.log(error);
        });
    }

    public onSubmit = ({ fields, errors, isValid }) => {
        console.log(fields);
        if (isValid) {
            this.assignToEmployee(fields);
        }
    }

    public assignToEmployee(value) {
        console.log(value);
        // If suit is assigned already remove it from employee cert in OCS
        if (this.state.data.assignedTo && this.state.data.assignedTo !== "Unassigned") {
            console.log("Remove it from employee")
            const payload = {
                suitId: this.state.data.id,
                validTo: this.state.data.nextService
            }
            // Remove certificate from user
            axios.post(Config.server + '/cert/' + value.user[0].EmployeeID + '/removeCert', payload).then(
                (response) => {
                    console.log(response);
                }).catch((error) => {
                    console.log(error);
                });
        }


        const payload = {
            suitId: this.state.data.id,
            validTo: this.state.data.nextService
        }
        console.log(payload);
        // Add certificate for user
        axios.post(Config.server + '/cert/' + value.user[0].EmployeeID + '/addCert', payload).then(
            (response) => {
                console.log(response);

                // Assign suit
                axios.post(Config.server + '/suit/' + this.state.data.id + '/assign', value.user).then(
                    (response) => {
                        console.log(response);
                        this.props.history.push({
                            pathname: '/result',
                            state: { id: this.state.data.id }
                        })

                    }).catch((error) => {
                        console.log(error);
                    });


            }).catch((error) => {
                console.log(error);
            });





    }

    public render() {
        if (!this.state.data.id) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <FormValidation onSubmit={this.onSubmit} config={this.formConfig}>
                                        {({ fields, errors, submitted, setField }) => (
                                            <>
                                                <h2>Assign suit {this.state.data.id} to employee</h2>

                                                <AsyncTypeahead
                                                    labelKey={option => `${option.EmployeeID}: ${option.EmployeeName}`}
                                                    multiple={false}
                                                    options={this.state.employees}
                                                    minLength={3}
                                                    onSearch={this.handleSearch}
                                                    isLoading={this.state.loading}
                                                    placeholder="Search for Employee..."
                                                    name="id"
                                                    isInvalid={submitted && errors.id ? true : false}
                                                    onChange={(value) => { console.log(value); setField({ id: value.EmployeeId, user: value }); }}
                                                />

                                                <button type="submit" className="button">Assign</button>
                                                <Link to={{
                                                    pathname: '/result',
                                                    state: { id: this.state.data.id }
                                                }}>
                                                    <button className="button">Cancel</button>
                                                </Link>
                                            </>
                                        )}

                                    </FormValidation>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AssignSuitEmployee;