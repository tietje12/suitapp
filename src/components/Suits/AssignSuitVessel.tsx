import { FormValidation } from 'calidation';
import * as React from 'react';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

import axios from 'axios';


class AssignSuitVessel extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            allowNew: false,
            loading: false,
            vessels: []
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.assignToVessel = this.assignToVessel.bind(this);
        this.handleSearch = this.handleSearch.bind(this);

        this.formConfig = {
            id: {
                isRequired: 'Vessel is required'
            }
        }
    }

    public componentWillMount() {
        this.getSuit();
    }

    public getSuit(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }

    public handleSearch = (query) => {
        this.setState({ loading: true });

        axios.post(Config.server + '/vessel/find', { "value": query }).then(response => {
            console.log(response);
            this.setState({
                vessels: response.data,
                loading: false
            })
        }).catch((error) => {
            console.log(error);
        });
    }


    public onSubmit = ({ fields, errors, isValid }) => {
        console.log("onSubmit", fields, errors, isValid);
        if (isValid) {
            this.assignToVessel(fields);
        }
    }

    public assignToVessel(value) {
    console.log(value);
         // If suit is assigned already remove it from employee cert in OCS
         if (this.state.data.assignedTo && this.state.data.assignedTo !== "Unassigned" && this.state.data.assignedTo.hasOwnProperty("Guest")) {
            console.log("Remove it from employee")
            const payload = {
                suitId: this.state.data.id,
                validTo: this.state.data.nextService
            }
            // Remove certificate from user
            axios.post(Config.server + '/cert/' + value.user[0].EmployeeID + '/removeCert', payload).then(
                (response) => {
                    console.log(response);
                }).catch((error) => {
                    console.log(error);
                });
        }

        const payload : string[] = [];
        payload[0] = value;
        console.log(this.state.data);
        axios.post(Config.server + '/suit/' + this.state.data.id + '/assign', payload).then(
            (response) => {
                console.log(response);
                this.props.history.push({
                    pathname: '/result',
                    state: { id: this.state.data.id }
                }) 

            }).catch((error) => {
                console.log(error);
            });

    }

    public render() {
        if (!this.state.data.id) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <FormValidation onSubmit={this.onSubmit} config={this.formConfig}>
                                        {({ fields, errors, submitted, setField }) => (
                                            <>
                                                <h2>Assign suit {this.state.data.id} to vessel</h2>

                                                <AsyncTypeahead
                                                    labelKey={option => `${option.name}`}
                                                    multiple={false}
                                                    options={this.state.vessels}
                                                    minLength={3}
                                                    onSearch={this.handleSearch}
                                                    isLoading={this.state.loading}
                                                    placeholder="Search for Vessel..."
                                                    name="id"
                                                    isInvalid={submitted && errors.id ? true : false}
                                                    onChange={(value) => {setField({id: value.length ? value[0]._id : "", name: value[0].name, vessel: true}) } }
                                                />
                                               
                                                <button type="submit" className="button">Assign to vessel</button>
                                                <Link to={{
                                                    pathname: '/result',
                                                    state: { id: this.state.data.id }
                                                }}>
                                                    <button className="button">Cancel</button>
                                                </Link>
                                            </>
                                        )}

                                    </FormValidation>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AssignSuitVessel;