import * as React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

// Components
import LoadingAnimation from '../Animation/LoadingAnimation';

// Axios for HTTP requests
import axios from 'axios';

class AvailableSuits extends React.Component<any, any>  {

  constructor(props: any) {
    super(props);
    this.state = {
      name: "Esvagt",
      data: [],
      loading: false,
      suitTypes: [],
      locations: []
    };

    // Remember to bind "this." methods
    this.getSuits = this.getSuits.bind(this);
    this.locationFormatter = this.locationFormatter.bind(this);
    this.suitTypeFormatter = this.suitTypeFormatter.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  public componentDidMount() {
    Promise.all([this.getLocations(), this.getSuitTypes()]).then(() => this.getSuits());
  }

  public onClick(e, row, rowIndex) {
    if (!row.id) {
      return;
    }

    this.props.history.push({
      pathname: '/result',
      state: { id: row.id }
    });
  }

  // Get all available suits
  public getSuits(): void {
    this.setState({
      loading: true
    });

    axios.get(Config.server + '/suit/available')
      .then(
        response => {
          if (response.data === "No matching results found.") {
            this.setState({
              data: [],
              loading: false
            })
          } else {
            this.setState({
              data: response.data,
              loading: false
            });
          }
        }
      )
  }

  public getLocations(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/location')
      .then(
        response => {
          this.setState({
            locations: response.data,
            loading: false
          })
        }
      )
  }

  public getSuitTypes(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/suitType')
      .then(
        response => {
          this.setState({
            suitTypes: response.data,
            loading: false
          })
        }
      )
  }

  public locationFormatter(cell, row) {
    const location = this.state.locations.find(x => x._id === cell);
    if (location) {
      return location.title;
    }

    return "";
  }

  public suitTypeFormatter(cell, row) {
    const suitType = this.state.suitTypes.find(x => x._id === cell);
    if (suitType) {
      return suitType.title;
    }

    return "";
  }

  public render() {

    const columns = [
      {
        dataField: 'id',
        text: 'ID',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'size',
        text: 'Size',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'shoeSize',
        text: 'Shoe Size',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'year',
        text: 'Year',
        sort: true,
        filter: textFilter()
      },
      {
        dataField: 'suitType',
        text: 'Type',
        formatter: this.suitTypeFormatter,
        filter: textFilter(),
        filterValue: this.suitTypeFormatter,
        sort: true
      },
      {
        dataField: 'location',
        text: 'Location',
        formatter: this.locationFormatter,
        filter: textFilter(),
        filterValue: this.locationFormatter,
        sort: true
      }
    ];

    const defaultSort = [{
      dataField: 'id',
      order: 'asc'
    }];

    const rowEvents = {
      onClick: this.onClick
    };

    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  {this.state.loading &&
                    <LoadingAnimation />
                  }
                  {this.state.data && this.state.data.length ?
                    <BootstrapTable keyField='_id' rowEvents={rowEvents} data={this.state.data} bootstrap4={true} columns={columns} striped={true} condensed={true} hover={true} bordered={false} defaultSorted={defaultSort} filter={filterFactory()} noDataIndication="No available suits." />
                    : <h2 className="box-center">No available suits</h2>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AvailableSuits;
