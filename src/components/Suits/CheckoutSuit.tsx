import { FormValidation } from 'calidation';
import * as React from 'react';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';
import DropdownObjects from '../Dropdown/DropdownObjects';
import Textarea from '../Inputs/Textarea';

import axios from 'axios';

class CheckoutSuit extends React.Component<any, any>  {
    private formConfig: any;
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            location: "",
            inUseTempComment: "",
        };

        this.formConfig = {
            location: {
                isRequired: 'Location is required'
            },
            checkOutComment: {
            }
        }
    }

    public componentWillMount() {
        this.getSuit();
        this.getLocations();
    }

    public getSuit(): void {
        this.setState({  
            loading: true
        })
        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }

    public getLocations(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/location')
            .then(
                response => {
                    this.setState({
                        locations: response.data,
                        loading: false,
                        location: response.data ? response.data[0]._id : ""
                    });
                }
            )
    }

    public onSubmit = ({ fields, errors, isValid }) => {
        if (isValid) {
            this.checkoutSuit(fields);
        }
    }

    public checkoutSuit(fields) {
        console.log(fields)
        axios.post(Config.server + '/suit/' + this.state.data.id + '/checkout', {"location": fields.location, "checkOutComment": fields.checkOutComment, "inUseTempComment": this.state.inUseTempComment}).then(
            () => {
                this.props.history.push({
                    pathname: '/result',
                    state: { id: this.state.data.id }
                })

            }).catch((error) => {
                console.log(error);
            });
    }

    public render() {
        if (!this.state.data.id) { 
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <FormValidation onSubmit={this.onSubmit} config={this.formConfig}>
                            {({ fields, errors, submitted, setField }) => (
                                <>
                                    <div className="row">
                                        <div className="col-sm-12"> 
                                            <div className="box">
                                                <h2>Checkout suit {this.state.data.id}</h2>

                                                <DropdownObjects items={this.state.locations} label="Location" labelName="location" name="location" value={fields.location} onChange={(event) => {setField({location: event.target.value}); this.setState({location: event.target.value}) }} />
                                                <Textarea placeholder="Enter a comment" label="comment" labelName="Comment" name="checkOutComment" error={submitted && errors.checkOutComment ? errors.checkOutComment : ''} />
                                                {this.state.location === "5c769806982d4a7969a3859b" &&
                                                <Textarea placeholder="Enter a employee id" label="Empoloyee ID" labelName="Empoloyee ID" name="inUseTempComment" error={submitted && errors.inUseTempComment ? errors.inUseTempComment : ''} onChange={(event) => {setField({inUseTempComment: event.target.value}); this.setState({inUseTempComment: event.target.value}) }} />
                                            
                                            }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div className="box">
                                                

                                                <button type="submit">Checkout</button>

                                                <Link to={{
                                                    pathname: '/result',
                                                    state: { id: this.state.data.id }
                                                }}>
                                                    <button className="button">Cancel</button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )}
                        </FormValidation>
                    </div>
                </div>
            </div>
        );
    }
}

export default CheckoutSuit;