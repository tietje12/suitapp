import { FormValidation } from 'calidation';
import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Components
import Dropdown from '../Dropdown/Dropdown';
import DropdownObjects from '../Dropdown/DropdownObjects';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import LocationModal from '../Location/LocationModal';
import ActionNotification from '../Notifications/ActionNotification';
import SuitTypeModal from '../SuitType/SuitTypeModal';

// Datepicker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// Axios for HTTP requests
import axios from 'axios';

class CreateSuit extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            suitTypes: [],
            location: [],
            suit: {
                id: this.props.location.state.id,
                type: this.props.location.state.type,
                nextService: new Date()
            },
            isOpen: false,
            isLocationOpen: false,
            success: false
        };

        this.formConfig = {
            year: {
                isRequired: 'Year is required',
            },
            vessel: {
                
            },
            assignedTo: {

            },
            size: {

            },
            shoeSize: {

            },
            location: {
                isRequired: 'Location is required'
            },
            nextService: {

            },
            comment: {

            }
        }

        // Bind functions
        this.createSuit = this.createSuit.bind(this);
        this.getSuitTypes = this.getSuitTypes.bind(this);
        this.getLocations = this.getLocations.bind(this);
        this.showSuitTypeModal = this.showSuitTypeModal.bind(this);
        this.showLocationModal = this.showLocationModal.bind(this);
        this.closeSuitTypeModal = this.closeSuitTypeModal.bind(this);
        this.closeLocationModal = this.closeLocationModal.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
    }

    public showSuitTypeModal() {
        this.setState({
            isOpen: true
        })
    }
    public showLocationModal() {
        this.setState({
            isLocationOpen: true
        })
    }

    public closeSuitTypeModal() {
        this.setState({
            isOpen: false
        })
    }
    public closeLocationModal() {
        this.setState({
            isLocationOpen: false
        })
    }

    public componentDidMount() {
        this.getSuitTypes();
        this.getLocations();
    }

    public closeNotification() {
        this.setState({
            success: false
        });
        this.props.history.push({
            pathname: '/result',
            state: { id: this.state.resultResponse.data.id }
        })
    }

    public createSuit = ({ fields, errors, isValid }) => {
        if (isValid) {
            const updatedSuit = Object.assign({}, this.state.suit, fields);

            axios.post(Config.server + '/suit/', updatedSuit).then(response => {
                console.log(response)
                this.setState({
                    suit: response.data,
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "A new suit has succesfully been created. Please proceed by clicking on ok."
                })
                this.removeUnknownSuit(this.state.suit.id);

                this.props.history.push({
                    pathname: '/result',
                    state: { id: response.data.id }
                })
            }).catch((error) => {
                console.log(error);
                this.setState({
                    success: true,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });
        }
    }

    // If suit is unknown remove it from unkown suits table
    public removeUnknownSuit(id): void {
        axios.delete(Config.server + '/unknown/+', id).then(
            response => {
                console.log(response);
            }).catch((error) => {
                console.log(error);
            });
    }

    public getSuitTypes(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suitType')
            .then(
                response => {
                    this.setState({
                        suitTypes: response.data,
                        loading: false,
                        isOpen: false
                    })
                }
            )
    }

    public getLocations(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/location')
            .then(
                response => {
                    this.setState({
                        location: response.data,
                        loading: false,
                        isLocationOpen: false
                    })
                }
            )
    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-8">
                                <FormValidation onSubmit={this.createSuit} config={this.formConfig} initialValues={{ shoeSize: "", size: "", nextService: this.state.suit.nextService, location: this.state.location.length ? this.state.location[0]._id : undefined, suitType: this.state.suitTypes ? this.state.suitTypes[0] : undefined }}>
                                    {({ fields, errors, submitted, setField }) => (
                                        <>
                                            <div className="box">
                                                <h2>Create new {this.state.suit.type}</h2>
                                                <h5>ID: {this.state.suit.id}</h5>
                                                <DropdownObjects items={this.state.suitTypes} label="Suit Type" labelName="suitType" name="suitType" value={fields.suitType} onChange={(event) => setField({ suitType: event.target.value })} />
                                                <button onClick={this.showSuitTypeModal} className="button-small">Create new suittype</button>
                                                <DropdownObjects items={this.state.location} label="Location" labelName="location" name="location" value={fields.location} onChange={(event) => setField({ location: event.target.value })} />
                                                <button onClick={this.showLocationModal} className="button-small">Create new location</button>
                                                <Dropdown items={["XS", "S", "M", "L", "XL", "XXL", "XXXL", "180N", "275N"]} label="Size" labelName="size" name="size" value={fields.size} onChange={(event) => setField({ size: event.target.value })} />
                                                <InputAsync placeholder="Enter year" label="Year" labelName="year" name="year" value={fields.year} error={submitted && errors.year ? errors.year : ''} />
                                                {this.state.suit.type === "survival" && <Dropdown items={["36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51"]} label="Shoe Size" labelName="showSize" name="showSize" value={fields.shoeSize} onChange={(event) => setField({ shoeSize: event.target.value })} />}
                                                <InputAsync placeholder="Enter vessel" label="Vessel" labelName="vessel" name="vessel" value={fields.vessel} error={submitted && errors.vessel ? errors.vessel : ''} />
                                                <div>
                                                    <p className="input-label col-form-label">Next service</p>
                                                    <DatePicker
                                                        selected={fields.nextService}
                                                        onSelect={(newDate) => setField({ nextService: newDate })}
                                                        placeholderText="Select next service date"
                                                        dateFormat="dd-MM-yyyy"
                                                    />
                                                </div>
                                                <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" value={fields.comment} error={submitted && errors.comment ? errors.comment : ''} />

                                                <button type="submit">Create Suit</button>
                                                {this.state.success &&
                                                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                                                }
                                            </div>
                                        </>
                                    )}
                                </FormValidation>
                            </div>

                        </div>
                    </div>
                </div>
                {this.state.isOpen &&
                    <SuitTypeModal close={this.closeSuitTypeModal} update={this.getSuitTypes} />
                }
                {this.state.isLocationOpen &&
                    <LocationModal close={this.closeLocationModal} update={this.getLocations} />
                }
            </div>
        );

    }
}

export default CreateSuit;
