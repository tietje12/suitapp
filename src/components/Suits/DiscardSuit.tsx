import * as React from 'react';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';

import DropdownObjects from '../Dropdown/DropdownObjects';
import Textarea from '../Inputs/Textarea';

class DiscardSuit extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
            reasons: [],
            reason: "",
            comment: "",
        };

        // Remember to bind "this." handleChangeComment
        this.handleChangeReason = this.handleChangeReason.bind(this);
        this.handleChangeComment = this.handleChangeComment.bind(this);
        this.decomissionSuit = this.decomissionSuit.bind(this);
        this.reassignAsGuestSuit = this.reassignAsGuestSuit.bind(this);
    }

    public componentWillMount() {
        this.getSuit();
        this.getReasons();
    }

    public getSuit(): void {
        this.setState({
            loading: true
        });

        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            );
    }

    public getReasons(): void {
        this.setState({
            loading: true
        });

        axios.get(Config.server + '/decomission/reasons')
            .then(
                response => {
                    this.setState({
                        reasons: response.data,
                        reason: response.data.length ? response.data[0]._id : "",
                        loading: false
                    })
                }
            );
    }

    // Decomission suit
    public decomissionSuit(): void {
        // Assign this state to a new object
        const data = { ...this.state.data }
        data.decomissioningReason = this.state.reason;
        data.status = "Decomissioned";
        data.guestSuit = false;
        data.assignedTo = {};
        this.updateSuit(data);
    }

    // Change to Guest Suit
    public reassignAsGuestSuit(): void {
        // Assign this state to a new object
        const data = { ...this.state.data }
        data.decomissioningReason = this.state.reason;
        data.status = "Dormant";
        data.guestSuit = true;
        data.assignedTo = {};
        this.updateSuit(data);
    }

    public handleChangeReason = (event) => {
        this.setState({
            reason: event.target.value
        });
    };

    public handleChangeComment = (event) => {
        this.setState({
            comment: event.target.value
        });
    };


    public updateSuit(suit: any): void {
        axios.post(Config.server + '/suit/' + suit.id, suit).then(
            () => {
                this.props.history.push({
                    pathname: '/result',
                    state: { id: this.state.data.id }
                });

            }).catch((error) => {
                console.log(error);
            });
    }

    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Discard {this.state.data.type} {this.state.data.id}</h2>

                                    <DropdownObjects items={this.state.reasons} label="Reason" labelName="reason" name="reason" value={this.state.reason} onChange={this.handleChangeReason} />
                                    <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" onChange={this.handleChangeComment} />

                                    <button className="button" onClick={this.decomissionSuit}>Decomission</button>
                                    <button className="button" onClick={this.reassignAsGuestSuit}>Reassign as Guest Suit</button>
                                    <Link to={{
                                        pathname: '/result',
                                        state: { id: this.props.match.params.id }
                                    }}>
                                        <button className="button">Cancel</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DiscardSuit;