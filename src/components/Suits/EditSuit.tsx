import { FormValidation } from 'calidation';
import * as React from 'react';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';
import Dropdown from '../Dropdown/Dropdown';
import DropdownObjects from '../Dropdown/DropdownObjects';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';

// Datepicker
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class EditSuit extends React.Component<any, any>  {
  private formConfig: any;

  constructor(props: any) {
    super(props);
    this.state = {
      data: {},
      suit: {}
    };

    this.formConfig = {
      year: {
      },
      suitType: {

      },
      vessel: {
      },
      assignedTo: {

      },
      size: {

      },
      shoeSize: {

      },
      location: {
        isRequired: 'Location is required'
      },
      nextService: {

      },
      comment: {

      }
    }

    // Remember to bind "this."
    this.updateSuit = this.updateSuit.bind(this);
  }

  public componentWillMount() {
    this.getSuit();
    this.getSuitTypes();
    this.getLocations();
  }

  // Call parent function parsed as prop
  public updateSuit = ({ fields, errors, isValid }) => {
    if (isValid) {
      const updatedSuit = Object.assign({}, this.state.data, fields);
      
      axios.post(Config.server + '/suit/' + this.state.data.id, updatedSuit).then(
        () => {

          this.props.history.push({
            pathname: '/result',
            state: { id: this.state.data.id }
        })

        }).catch((error) => {
          console.log(error);
        });
    }
  }

  public getSuitTypes(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/suitType')
      .then(
        response => {
          this.setState({
            suitTypes: response.data,
            loading: false
          })
        }
      )
  }

  public getSuit(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/suit/' + this.props.match.params.id)
      .then(
        response => {
          this.setState({
            data: response.data,
            loading: false
          })
        }
      )
  }

  public getLocations(): void {
    this.setState({
      loading: true
    })
    axios.get(Config.server + '/location')
      .then(
        response => {
          this.setState({
            locations: response.data,
            loading: false
          })
        }
      )
  }



  // Handle change of date change
  public handleSelect(date) {
    // Make a copy of the object in the state
    const updatedValue = Object.assign({}, this.state.suit, { nextService: date });

    // Now we can update the state
    this.setState({ suit: updatedValue });
  }

  public onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  public render() {
    if (!this.state.data.id) {
      return null;
    }

    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <FormValidation onSubmit={this.updateSuit} config={this.formConfig} initialValues={this.state.data}>
                  {({ fields, errors, submitted, setField }) => (
                    <>
                      <div className="box">
                        <h2>Edit {this.state.data.type} {this.state.data.id}</h2>
                        <DropdownObjects items={this.state.suitTypes} label="Suit Type" labelName="suitType" name="suitType" value={fields.suitType} onChange={(event) => setField({ suitType: event.target.value })} />
                        <DropdownObjects items={this.state.location} label="Location" labelName="location" name="location" value={fields.location} onChange={(event) => setField({ location: event.target.value })} />
                        <Dropdown items={["XS", "S", "M", "L", "XL", "XXL", "XXXL", "180N", "275N"]} label="Size" labelName="size" name="size" value={fields.size} onChange={(event) => setField({ size: event.target.value })} />
                        <InputAsync placeholder="Enter year" label="Year" labelName="year" name="year" value={fields.year} error={submitted && errors.year ? errors.year : ''} />
                        <Dropdown items={["36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51"]} label="Shoe Size" labelName="showSize" name="showSize" value={fields.shoeSize} onChange={(event) => setField({ shoeSize: event.target.value })} />
              
                        {this.state.data.type === "survival" && <Dropdown items={["36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51"]} label="Shoe Size" labelName="showSize" name="showSize" value={fields.shoeSize} onChange={(event) => setField({ shoeSize: event.target.value })} /> }
                        <InputAsync placeholder="Enter vessel" label="Vessel" labelName="vessel" name="vessel" value={fields.vessel} error={submitted && errors.vessel ? errors.vessel : ''} />

                        <div>
                          <p className="input-label col-form-label">Next service</p>
                          <DatePicker
                            selected={fields.nextService}
                            onSelect={(newDate) => setField({ nextService: newDate })}
                            placeholderText="Select next service date"
                            dateFormat="dd-MM-yyyy"
                          />
                        </div>
                        <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" value={fields.comment} error={submitted && errors.comment ? errors.comment : ''} />

                        <button className="button" type="submit">Update</button>
                        <Link to={`/suit/${this.state.data.id}/discard`}>
                          <button className="button">Discard</button>
                        </Link>
                        <Link to={{
                          pathname: '/result',
                          state: { id: this.state.data.id }
                        }}>
                          <button className="button">Cancel</button>
                        </Link>
                      </div>
                    </>
                  )}
                </FormValidation>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditSuit;
