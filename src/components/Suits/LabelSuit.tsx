import * as React from 'react';
import { Link } from 'react-router-dom';
import {default as Config} from '../../config/config'
import '../../styles/App.css';

import axios from 'axios';

class LabelSuit extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
        };

        this.printSuitCertificate = this.printSuitCertificate.bind(this);
        this.printSuitLabel = this.printSuitLabel.bind(this);
    }

    public componentWillMount() {
        this.getSuit();
    }

    public getSuit(): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/suit/' + this.props.match.params.id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }

    public printSuitLabel() {
        // TODO: Print
    }

    public printSuitCertificate() {
        // TODO: Print
    }

    public render() {
        if (!this.state.data.id) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Print labels for suit {this.state.data.id}</h2>

                                    <button className="button" onClick={this.printSuitLabel}>Print Label</button>
                                    <button className="button" onClick={this.printSuitCertificate}>Print Certificate</button>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <Link to={{
                                        pathname: '/suit/' + this.state.data.id + '/checkout',
                                        state: { id: this.state.data.id }
                                    }}>
                                        <button className="button">Checkout</button>
                                    </Link>

                                    <Link to={{
                                        pathname: '/result',
                                        state: { id: this.state.data.id }
                                    }}>
                                        <button className="button">Cancel</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LabelSuit;