import { FormValidation } from 'calidation';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
// Components
import LoadingAnimation from '../Animation/LoadingAnimation';

// Axios for HTTP requests
import axios from 'axios';

class SearchSuit extends React.Component<any, any>  {
  private formConfig: any;
  private nameInput: any;

  constructor(props: any) {
    super(props);
    this.state = {
      name: "Esvagt",
      value: "",
      users: [],
      edit: false,
      data: null,
      loading: false,
    };

    // Remember to bind "this." methods
    this.getSuits = this.getSuits.bind(this);
    this.searchSuit = this.searchSuit.bind(this);

    this.formConfig = {
      suitNumber: {
        isRequired: 'Suit number is required'
      }
    }
  }


  public componentDidMount() {
    this.nameInput.focus();
  }

  public searchSuit = ({ fields, errors, isValid }) => {
    if (isValid) {
      const specialString = fields.suitNumber.substring(0, 5)
    console.log(specialString);

    if (specialString === '¨C121') {
      fields.suitNumber = fields.suitNumber.substring(5);
      console.log("Contains c121")
    }
      this.setState({
        value: fields.suitNumber
      });

      this.getSuits(fields.suitNumber);
    }
  }

  public keyDetect(e) {
    console.log(e);
  }

  // Get all suits
  public getSuits(id): void {
    const specialString = id.substring(0, 5)
    const specialStringSmall = id.substring(0, 4)
    console.log(specialString);
    console.log(specialStringSmall);

    if (specialString.toUpperCase() === '¨C121'.toUpperCase()) {
      id = id.substring(5);
      console.log("Contains c121")
    } 
    if (specialStringSmall.toUpperCase() === 'C121'.toUpperCase()) {
      id = id.substring(4);
      console.log("Contains c121")
    } 

    id = id.trim();
    // id = id.substring(4);
    this.nameInput.value = id;
    console.log(id)

    this.setState({
      loading: true
    })
    if (!id) {
      id = "0"
    }

    axios.get(Config.server + '/suit/' + id)
      .then(
        response => {
          if (response.data === "No matching results found.") {
            this.setState({
              data: response.data,
              loading: false
            })
          } else {
            this.props.history.push({
              pathname: '/result',
              state: { id: response.data.id }
            })
          }
        }
      )
  }

  public render() {
    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  <FormValidation onSubmit={this.searchSuit} config={this.formConfig}>
                    {({ fields, errors, submitted, setField }) => (
                      <>
                        <p className="input-label text-center">Enter Suit Number</p>
                        <input onKeyDown={this.keyDetect} ref={(input) => { this.nameInput = input; }} placeholder="Please scan the suit or enter the ID." name="suitNumber" className={submitted && errors.suitNumber ? 'is-invalid form-control text-center' : 'form-control text-center'} />
                        <div className="invalid-feedback">{errors.suitNumber}</div>
                        <button type="submit" className="align-center">Search</button>
                      </>
                    )}
                  </FormValidation>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="box">
                  {this.state.loading &&
                    <LoadingAnimation />
                  }
                  {this.state.data === "No matching results found." &&
                    <h2 className="box-center">No matching results found.</h2>

                  }
                  {this.state.data === "No matching results found." &&
                    <div className="row">
                      <Link to={{ pathname: "/createSuit/", state: { id: this.state.value, type: "survival" } }} className="col-sm-6"><button className="button align-center">Create new suit</button></Link>
                      <Link to={{ pathname: "/createSuit/", state: { id: this.state.value, type: "vest" } }} className="col-sm-6"><button className="button align-center">Create new vest</button></Link>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchSuit;
