import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';



// Components
import Input from '../Inputs/Input';


// Axios for HTTP requests
import axios from 'axios';

class CreateTask extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
    this.state = {
      value: '',
      success: false
    };

    // Bind functions
    this.handleChange = this.handleChange.bind(this);
    this.createTask = this.createTask.bind(this);
    this.closeNotification = this.closeNotification.bind(this);
  }

  public closeNotification() {
    this.setState({
      success: false
    });
    this.props.history.push({
      pathname: '/tasks'
    })
  }

  public createTask(): void {
    axios.post(Config.server + '/serviceTasks', this.state.task).then(
      response => {
        console.log(response);
        this.setState({
          success: true,
          resultResponse: response,
          resultTitle: "Success",
          resultMessage: "A new task has succesfully been created. Please proceed by clicking on ok."
        })
      }).catch((error) => {
        console.log(error);
        this.setState({
          success: true,
          resultTitle: "Error",
          resultMessage: error
        })
      });

  }

  // Handle change of value events
  public handleChange(event) {
    // Make a copy of the object in the state
    const updatedValue = Object.assign({}, this.state.task, { [event.target.name]: event.target.value });

    // Now we can update the state
    this.setState({ task: updatedValue });
  }

  public render() {
    return (
      <div className="App" >
        <div className="container">
          <div className="wrapper">
            <div className="row">
              <div className="col-sm-8">
                <div className="box" onChange={this.handleChange}>
                  <h2>Create new task</h2>
                  <Input placeholder="Enter Title" label="Title" labelName="title" name="title" />
                  <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment"/>
                  <button onClick={this.createTask}>Create task</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.success &&
          <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
        }
      </div>
    );

  }
}

export default CreateTask;
