import { FormValidation } from 'calidation';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { default as Config } from '../../config/config'
import '../../styles/App.css';

// Axios for HTTP requests
import axios from 'axios';
import InputAsync from '../Inputs/InputAsync';
import Textarea from '../Inputs/Textarea';
import ActionNotification from '../Notifications/ActionNotification';

class EditTask extends React.Component<any, any>  {
    private formConfig: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: {},
        };

        this.formConfig = {
            title: {

            },
            comment: {

            },
        }

        // Remember to bind "this."
        this.updateTask = this.updateTask.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
    }

    public closeNotification() {
        this.setState({
            success: false
        });
        if (this.state.resultTitle !== "Error") {

            this.props.history.push({
                pathname: '/task/' + this.props.location.state.data._id,
            })
        }

    }

    // Call parent function parsed as prop
    public updateTask = ({ fields, errors, isValid }) => {
        if (isValid) {
            const updatedTask = Object.assign({}, this.props.location.state.data, fields);

            axios.post(Config.server + '/serviceTasks/' + this.props.location.state.data._id, updatedTask).then(
                (response) => {

                    this.setState({
                        success: true,
                        resultResponse: response,
                        resultTitle: "Success",
                        resultMessage: "Task has been updated."
                    })

                }).catch((error) => {
                    this.setState({
                        success: true,
                        resultTitle: "Error",
                        resultMessage: error
                    })
                });
        }
    }










    public onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    public render() {
        if (!this.props.location.state.data) {
            return null;
        }

        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <FormValidation onSubmit={this.updateTask} config={this.formConfig} initialValues={this.props.location.state.data}>
                                    {({ fields, errors, submitted, setField }) => (
                                        <>
                                            <div className="box">
                                                <h2>Edit Task</h2>
                                                <InputAsync placeholder="Enter title" label="Title" labelName="title" name="title" value={fields.title} error={submitted && errors.title ? errors.title : ''} />
                                                <Textarea placeholder="Enter a comment" label="Comment" labelName="comment" name="comment" value={fields.comment} error={submitted && errors.comment ? errors.comment : ''} />

                                                <button className="button" type="submit">Update</button>
                                                <Link to={{
                                                    pathname: '/task/' + this.props.location.state.data.id,

                                                }}>
                                                    <button className="button">Cancel</button>
                                                </Link>
                                            </div>
                                        </>
                                    )}
                                </FormValidation>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default EditTask;
