import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';
import ActionNotification from '../Notifications/ActionNotification';

// Axios for HTTP requests
import axios from 'axios';


class SingleTask extends React.Component<any, any>  {
    constructor(props: any) {
        super(props);
        this.state = {
            value: '',
            data: {
            }
        };

        this.deleteTask = this.deleteTask.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.editTask = this.editTask.bind(this);

    }

    public closeNotification() {
        this.setState({
            success: false
        });
        this.props.history.push({
            pathname: '/tasks'
        })
    }

    public componentWillMount() {
        this.getTask(this.props.match.params.id);
    }

    public editTask() {
        this.props.history.push({
            pathname: '/task/' + this.state.data._id + '/edit',
            state: { data: this.state.data}
          })
    }
    // Delete task
    public deleteTask(): void {
        axios.delete(Config.server + '/serviceTasks/' + this.props.match.params.id).then(
            response => {
                console.log(response);
                this.setState({
                    success: true,
                    resultResponse: response,
                    resultTitle: "Success",
                    resultMessage: "Task has been deleted. Please proceed by clicking on ok."
                })

            }).catch((error) => {
                console.log(error);
                this.setState({
                    success: true,
                    resultTitle: "Error",
                    resultMessage: error
                })
            });
    }

    // Get single task
    public getTask(id): void {
        this.setState({
            loading: true
        })
        axios.get(Config.server + '/serviceTasks/' + id)
            .then(
                response => {
                    this.setState({
                        data: response.data,
                        loading: false
                    })
                }
            )
    }


    public render() {
        return (
            <div className="App" >
                <div className="container">
                    <div className="wrapper">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="box">
                                    <h2>Task: {this.state.data.title}</h2>
                                    <p className="input-label">Comment</p>
                                            <p>{this.state.data.comment}</p>
                                    <button className="button" onClick={this.editTask}>Edit task</button>
                                    <button className="button" onClick={this.deleteTask}>Delete task</button>

                                </div>
                            </div>
                        </div>



                    </div>
                </div>
                {this.state.success &&
                    <ActionNotification title={this.state.resultTitle} message={this.state.resultMessage} close={this.closeNotification} />
                }
            </div>
        );
    }
}

export default SingleTask;
