import * as React from 'react';
import { default as Config } from '../../config/config'
import '../../styles/App.css';


// Axios for HTTP requests
import axios from 'axios';
import { Link } from 'react-router-dom';

class Task extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
    this.state = { 
      name: "Esvagt",
      value: "",
      data: [],
      loading: false,   
    };


  }

  public componentDidMount() {
    this.getTasks();
    }

  public handleChange(event) {
    console.log(event);
    this.setState({value: event.target.value});
  }


    // Get all tasks
  public getTasks(): void{
    this.setState({
      loading:true
    })
    axios.get(Config.server + '/serviceTasks')
    .then(
        response =>{
          this.setState({
            data:response.data,
            loading: false
          })
        } 
      ) 
  }


  
  public render() {
    return (
        
      <div className="App" > 
        <div className="container">
          <div className="wrapper">
            <div className="row">
                  <div className="col-sm-12">
                      <div className="box">
                      <h2>Checklist Tasks</h2>
                      <ul className="infoList">
                        {this.state.data.map((item, index) =>
                            <li key={index}>
                                <Link to={`/task/${item._id}`}>
                                    {item.title} ({item.comment})
                                </Link>
                            </li>
                            

                        )}
                        </ul>
                        <Link to='/createTask'><button>Create new task</button></Link>
                      </div>
                  </div>
              </div>
          
          
          
          </div>
        </div>
      </div>
    );
  }
}

export default Task;
