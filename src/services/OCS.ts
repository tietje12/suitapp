import axios from 'axios';

class OCS {

    public getPersonnel(token){
        return axios({ method: 'get', url: 'https://satseaocs.esvagt.com:8099/restws/services/saatsea/personnel/ ', headers: { Authorization: `Bearer ${token}` } })
        .then(
            response =>{
                console.log(response)
              return response
            } 
          )
    }

    public getToken(){
        return axios.post('https://satseaocs.esvagt.com:8099/restws/services/token', 
            'UserName=SaatSea&Password=OCSSaatSea1122&grant_type=password')
        .then(
            response =>{
                console.log(response)
              return this.getPersonnel(response.data.access_token)
            } 
          )
    }

}

export default OCS;
